﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.UCs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginSocialButton : ContentView
    {
        #region Properties
        public string Icon
        {
            get { return base.GetValue(IconProperty).ToString(); }
            set { base.SetValue(IconProperty, value); }
        }
        public static readonly BindableProperty IconProperty = BindableProperty.Create(
                                                         propertyName: "Icon",
                                                         returnType: typeof(string),
                                                         declaringType: typeof(LoginSocialButton),
                                                         defaultValue: "",
                                                         defaultBindingMode: BindingMode.TwoWay,
                                                         propertyChanged: IconPropertyChanged);
        private static void IconPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (newValue != oldValue && newValue != null)
            {
                var control = (LoginSocialButton)bindable;
                control.icon.Source = newValue.ToString();
                if (newValue.ToString().Contains("facebook"))
                {
                    control.frame.BackgroundColor = Color.FromHex("1F53E9");
                    control.label.TextColor = Color.White;
                }
            }
        }
        public string Text
        {
            get { return base.GetValue(TextProperty).ToString(); }
            set { base.SetValue(TextProperty, value); }
        }
        public static readonly BindableProperty TextProperty = BindableProperty.Create(
                                                         propertyName: "Text",
                                                         returnType: typeof(string),
                                                         declaringType: typeof(LoginSocialButton),
                                                         defaultValue: "",
                                                         defaultBindingMode: BindingMode.TwoWay,
                                                         propertyChanged: TextPropertyChanged);
        private static void TextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (newValue != oldValue && newValue != null)
            {
                var control = (LoginSocialButton)bindable;
                control.label.Text = newValue.ToString();
            }
        }
        #endregion
        public LoginSocialButton()
        {
            InitializeComponent();
        }
    }
}