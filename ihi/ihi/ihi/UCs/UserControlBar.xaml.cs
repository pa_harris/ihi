﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.UCs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserControlBar : ContentView
    {
        #region Properties
        public Color BackgroundColor
        {
            get { return (Color)base.GetValue(BackgroundColorProperty); }
            set { base.SetValue(BackgroundColorProperty, value); }
        }
        public static readonly BindableProperty BackgroundColorProperty = BindableProperty.Create(
                                                         propertyName: "BackgroundColor",
                                                         returnType: typeof(Color),
                                                         declaringType: typeof(UserControlBar),
                                                         defaultValue: Color.FromHex("#1F53E9"),
                                                         defaultBindingMode: BindingMode.TwoWay,
                                                         propertyChanged: BackgroundColorPropertyChanged);
        private static void BackgroundColorPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (newValue != oldValue && newValue != null)
            {
                var control = (UserControlBar)bindable;
                control.absLayout.BackgroundColor = (Color)newValue;
                control.label.TextColor = Color.Black;
                control.label.FontAttributes = FontAttributes.Bold;
            }
        }
        public bool IsBackVisible
        {
            get { return (bool)base.GetValue(IsBackVisibleProperty); }
            set { base.SetValue(IsBackVisibleProperty, value); }
        }
        public static readonly BindableProperty IsBackVisibleProperty = BindableProperty.Create(
                                                         propertyName: "IsBackVisible",
                                                         returnType: typeof(bool),
                                                         declaringType: typeof(UserControlBar),
                                                         defaultValue: true,
                                                         defaultBindingMode: BindingMode.TwoWay,
                                                         propertyChanged: IsBackVisiblePropertyChanged);
        private static void IsBackVisiblePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (newValue != oldValue && newValue != null)
            {
                var control = (UserControlBar)bindable;
                control.btnBack.IsVisible = (bool)newValue;
            }
        }
        public int FontSize
        {
            get { return (int)base.GetValue(FontSizeProperty); }
            set { base.SetValue(FontSizeProperty, value); }
        }
        public static readonly BindableProperty FontSizeProperty = BindableProperty.Create(
                                                         propertyName: "FontSize",
                                                         returnType: typeof(int),
                                                         declaringType: typeof(UserControlBar),
                                                         defaultValue: 20,
                                                         defaultBindingMode: BindingMode.TwoWay,
                                                         propertyChanged: FontSizePropertyChanged);
        private static void FontSizePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (newValue != oldValue && newValue != null)
            {
                var control = (UserControlBar)bindable;
                control.label.FontSize = Convert.ToInt32(newValue);
            }
        }
        public int Height
        {
            get { return (int)base.GetValue(HeightProperty); }
            set { base.SetValue(HeightProperty, value); }
        }
        public static readonly BindableProperty HeightProperty = BindableProperty.Create(
                                                         propertyName: "Height",
                                                         returnType: typeof(int),
                                                         declaringType: typeof(UserControlBar),
                                                         defaultValue: 60,
                                                         defaultBindingMode: BindingMode.TwoWay,
                                                         propertyChanged: HeightPropertyChanged);
        private static void HeightPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (newValue != oldValue && newValue != null)
            {
                var control = (UserControlBar)bindable;
                control.absLayout.HeightRequest = Convert.ToInt32(newValue);
            }
        }
        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(UserControlBar));
        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
            }
        }
        #endregion
        #region Commands
        public ICommand OnClickBack { get; set; }
        #endregion
        public UserControlBar()
        {
            InitializeComponent();
            FirstLoad();
        }

        public void FirstLoad()
        {
            LoadCommand();
        }

        public void LoadCommand()
        {
            OnClickBack = new Command(async () =>
            {
                await Application.Current.MainPage.Navigation.PopAsync();
            });
        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }
    }
}