﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class SaveFcmDeviceTokenViewModel
    {
        public string NewToken { get; set; }
        public string OldToken { get; set; }
        public string Platform { get; set; }
    }
}
