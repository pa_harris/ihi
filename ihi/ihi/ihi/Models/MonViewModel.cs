﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class MonViewModel
    {
        public string MonId { get; set; }
        public string LopId { get; set; }
        public string Tieude { get; set; }
        public bool Trangthai { get; set; }
    }
}
