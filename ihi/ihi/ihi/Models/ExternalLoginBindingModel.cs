﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class ExternalLoginBindingModel
    {
        public string LoginProvider { get; set; }
        public string ExternalAccessToken { get; set; }
    }
}
