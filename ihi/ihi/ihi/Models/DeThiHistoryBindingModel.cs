﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class DeThiHistoryBindingModel
    {
        public string HocTapId { get; set; }
        public string Tieude { get; set; }
        public int Thoigianthi { get; set; }
        public int? SoCau { get; set; }
        public string FileDeThi { get; set; }
        public double? Point { get; set; }
        public EDeThiType DeThiType { get; set; }
    }
}
