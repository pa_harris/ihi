﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class RemoveFcmDeviceTokenViewModel
    {
        public string Token { get; set; }
    }
}
