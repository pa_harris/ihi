﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class BaiGiangBindingModel
    {
        public string BaigiangId { get; set; }
        public int? Thutu { get; set; }
        public string BaigiangTitle { get; set; }
        public string NoidungGioiThieu { get; set; }
    }
}
