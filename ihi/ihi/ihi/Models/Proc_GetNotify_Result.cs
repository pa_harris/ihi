﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public partial class Proc_GetNotify_Result
    {
        public string TargetId1 { get; set; }
        public string TargetId2 { get; set; }
        public short NotifyType { get; set; }
        public string Title { get; set; }
        public short? TargetType { get; set; }
        public string Body { get; set; }
        public string UserIdSend { get; set; }
        public System.DateTime CreateTime { get; set; }
        public string Avatar { get; set; }
        public string FullName { get; set; }
        public bool IsRead { get; set; }
    }
}
