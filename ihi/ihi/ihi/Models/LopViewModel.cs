﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class LopViewModel
    {
        public string LopId { get; set; }
        public string Tieude { get; set; }
        public bool Trangthai { get; set; }
    }
}
