﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class ErrorRespone
    {
        public string Message { get; set; }
        public object ModelState { get; set; }
    }
    public static class ModelStateError
    {
        public static string DesearializeModelStateError(ErrorRespone error)
        {
            if (error != null)
            {
                List<string> listError = new List<string>();
                string errorResult = "";

                var modelStateString = error.ModelState.ToString();
                var listModelStateError = JsonConvert.DeserializeObject<Dictionary<string, object>>(modelStateString);
                foreach (var item in listModelStateError)
                {
                    listError.AddRange(JsonConvert.DeserializeObject<string[]>(item.Value.ToString()));
                }
                foreach (var item in listError)
                {
                    errorResult += item + " ";
                }
                return errorResult;
            }
            return "";
        }
        

    }
}
