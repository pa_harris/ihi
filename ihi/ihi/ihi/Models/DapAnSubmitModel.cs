﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class DapAnSubmitModel
    {
        public string CauHoiId { get; set; }
        public int? DapAn { get; set; }
    }
}
