﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    class Enums
    {
    }
    public enum EOTPActionType
    {
        AccountRegister = 1,
        AccountForgotPassword
    }
    public enum LoginProvider
    {
        Facebook,
        Google,
        Apple
    }
    public enum ETrangThaiPhatHanh : byte
    {
        ChuaPhatHanh = 0,
        DangPhatHanh,
        DaHoanThanh
    }
    public enum EDeThiType
    {
        OnTap = 0,
        DeThi
    }
    public enum ESlideLayout
    {
        Left = 1,
        Right,
        Center
    }
}
