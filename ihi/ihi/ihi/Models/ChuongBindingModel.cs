﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class ChuongBindingModel
    {
        public string MuclucId { get; set; }
        public string MucLucTitle { get; set; }
        public int? MucLucThutu { get; set; }
        public ETrangThaiPhatHanh? MucLucTrangThai { get; set; }
        public int? BaiGiangCount { get; set; }
        public int? PracticeCount { get; set; }
        public int CauHoiCount { get; set; }
    }
}
