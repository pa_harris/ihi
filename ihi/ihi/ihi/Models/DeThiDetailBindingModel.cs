﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class DeThiDetailBindingModel
    {
        public string Id { get; set; }
        public string Tieude { get; set; }
        public int Thoigianthi { get; set; }
        public int? SoCau { get; set; }
        public EDeThiType DeThiType { get; set; }
        public List<Proc_GetListCauHoiDeThi_Result> CauHois { get; set; }
        public List<Proc_GetListCauHoiDapAnDeThi_Result> DapAns { get; set; }
    }
    public partial class Proc_GetListCauHoiDeThi_Result
    {
        public string CauhoiId { get; set; }
        public string BaigiangId { get; set; }
        public string BaitapId { get; set; }
        public string OntapId { get; set; }
        public string DethiId { get; set; }
        public string Filecauhoi { get; set; }
        public string Noidung { get; set; }
        public string Huongdan { get; set; }
        public string Filehuongdan { get; set; }
        public double Diem { get; set; }
        public byte MucDo { get; set; }
        public byte HinhThuc { get; set; }
        public DateTime? Ngaytao { get; set; }
        public int? Thutu { get; set; }
        public bool Trangthai { get; set; }
        public string Ma { get; set; }
        public string CreatedUserId { get; set; }
        public int TotalComment { get; set; }
        public DateTime? ActiveTime { get; set; }
        public int TotalView { get; set; }
        public int TotalLike { get; set; }
    }
    public partial class Proc_GetListCauHoiDapAnDeThi_Result
    {
        public string DapancauhoiId { get; set; }
        public string CauhoiId { get; set; }
        public string Giatri { get; set; }
        public bool? Phuongandung { get; set; }
        public bool UserDapAn { get; set; }
        public int? Thutu { get; set; }
    }
    public class Proc_GetListCauHoiDapAnWithUserDeThi_Result : Proc_GetListCauHoiDapAnDeThi_Result
    {
        public string Traloi { get; set; }
    }
}
