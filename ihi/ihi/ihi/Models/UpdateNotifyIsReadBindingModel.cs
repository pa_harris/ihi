﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class UpdateNotifyIsReadBindingModel
    {
        public string TargetId1 { get; set; }
        public string TargetId2 { get; set; }
        public short TargetType { get; set; }
    }
}
