﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class BaiGiangDetailedModel
    {
        public string BaigiangId { get; set; }
        public string BaigiangTitle { get; set; }
        //public string UrlVideo { get; set; }
        public bool BaigiangXemThu { get; set; }
        public int? BaigiangLimit { get; set; }
        public DateTime? BaigiangPublishedTime { get; set; }
        public TimeSpan? VideoDuration { get; set; }
    }
}
