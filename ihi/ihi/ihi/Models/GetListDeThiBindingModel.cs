﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class GetListDeThiBindingModel
    {
        public EDeThiType DeThiType { get; set; }
        public string MucLucId { get; set; }
        public string BaiGiangId { get; set; }
        public string LopId { get; set; }
        public string MonId { get; set; }
        public int? page { get; set; } = 1;
        public int? pageSize { get; set; } = 5;
    }
}
