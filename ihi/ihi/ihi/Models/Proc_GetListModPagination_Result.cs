﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class Proc_GetListModPagination_Result
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Avatar { get; set; }
        public string Living { get; set; }
        public string Phone { get; set; }
    }
}
