﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class AccountLoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
