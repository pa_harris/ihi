﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public partial class Proc_GetTruong_Result
    {
        public int? IdTruong { get; set; }
        public string Name { get; set; }
        public int? IdCapHoc { get; set; }
        public int? IdQuanHuyen { get; set; }
    }
    public partial class Proc_GetTinhThanh_Result
    {
        public int? IdTinhThanhPho { get; set; }
        public string Name { get; set; }
    }
    public partial class Proc_GetQuanHuyen_Result
    {
        public int? IdQuanHuyen { get; set; }
        public string Name { get; set; }
        public int? IdTinhThanhPho { get; set; }
    }
    public partial class Proc_GetKhoi_Result
    {
        public int? IdKhoiThi { get; set; }
        public string Name { get; set; }
    }
    public partial class Proc_GetCapHoc_Result
    {
        public int? IdCapHoc { get; set; }
        public string Name { get; set; }
    }
}
