﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class ConfirmOtpModel
    {
        public string Email { get; set; }
        public string Fullname { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Otp { get; set; }
    }
}
