﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class PopularCourseBindingModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int? Order { get; set; }
        public string UrlImg { get; set; }
        public string LopId { get; set; }
        public string MonId { get; set; }
        public int? ParentId { get; set; }
        public string Lop { get; set; }
        public int? LopOrder { get; set; }
    }
}
