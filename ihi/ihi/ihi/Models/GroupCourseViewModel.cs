﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class GroupCourseViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Tieude { get; set; }
        public string Description { get; set; }
        public string UrlImg { get; set; }
        public string LopId { get; set; }
        public string MonId { get; set; }
        public int? ParentId { get; set; }
    }
}
