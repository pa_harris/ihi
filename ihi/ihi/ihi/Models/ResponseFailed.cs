﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class ResponseFailed
    {
        public string error { get; set; }
        public string error_description { get; set; }
    }
    public class MessageFailed
    {
        public string Message { get; set; }
    }
}
