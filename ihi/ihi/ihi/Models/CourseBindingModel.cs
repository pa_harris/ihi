﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class CourseBindingModel
    {
        public string KhoahocId { get; set; }
        public string Tieude { get; set; }
        public string Mota { get; set; }
        public DateTime? Ngaytao { get; set; }
        public DateTime? Ngaycapnhat { get; set; }
        public decimal? Gia { get; set; }
        public double? Chietkhau { get; set; }
        public string UrlImg { get; set; }
        public string UrlVideo { get; set; }
        public string SEO { get; set; }
        public int? Luotxem { get; set; }
        public string StreamUrl { get; set; }
        public string FullName { get; set; }
        public string UserId { get; set; }
        public string Mon { get; set; }
        public int? CountOrder { get; set; }
    }
}
