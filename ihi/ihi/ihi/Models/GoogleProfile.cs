﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class GoogleProfile
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public Uri Picture { get; set; }
    }
}
