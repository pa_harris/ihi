﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class ResendOtpModel
    {
        public string Email { get; set; }
        public EOTPActionType ActionType { get; set; }
    }
}
