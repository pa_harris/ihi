﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class ProfileBasesViewModel
    {
        public string Email { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Living { get; set; }
        public string Avatar { get; set; }
        public string Banner { get; set; }
        public string Phone { get; set; }
        public int? IdCapHoc { get; set; }
        public int? IdTruong { get; set; }
        public int? IdQuanHuyen { get; set; }
        public int? IdTinhThanhPho { get; set; }
        public byte? Khoi { get; set; }
        public int? NienKhoa { get; set; }
        public string Lop { get; set; }
        public decimal Money { get; set; }
    }
}
