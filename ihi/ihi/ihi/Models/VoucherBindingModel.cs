﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class VoucherBindingModel
    {
        public string Id { get; set; }
        public decimal GiaTri { get; set; }
        public byte LoaiGiam { get; set; }
        public DateTime? NgayTao { get; set; }
        public DateTime? NgayHetHan { get; set; }
        public DateTime? NgayBan { get; set; }
        public bool Block { get; set; }
        public string IdCongTacVien { get; set; }
        public DateTime? NgayCap { get; set; }
        public byte? TinhTrangBan { get; set; }
        public string TargetId { get; set; }
        public short TargetType { get; set; }
        public int Quantity { get; set; }
        public short PublicType { get; set; }
    }
}
