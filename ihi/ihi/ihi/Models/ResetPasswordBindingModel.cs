﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class ResetPasswordBindingModel
    {
        public string Email { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string OTP { get; set; }
    }
}
