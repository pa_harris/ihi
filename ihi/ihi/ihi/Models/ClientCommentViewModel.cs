﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class ClientCommentViewModel
    {
        public string Purport { get; set; }
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string Living { get; set; }
        public string Avatar { get; set; }
    }
}
