﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class DeThiHistoryDetailBindingModel
    {
        public string HocTapId { get; set; }
        public string Tieude { get; set; }
        public int Thoigianthi { get; set; }
        public int? SoCau { get; set; }
        public EDeThiType DeThiType { get; set; }
        public List<Proc_GetListCauHoiDeThi_Result> CauHois { get; set; }
        public List<Proc_GetListCauHoiDapAnWithUserDeThi_Result> DapAns { get; set; }
    }
}
