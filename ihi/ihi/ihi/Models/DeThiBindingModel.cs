﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class DeThiBindingModel
    {
        public string Id { get; set; }
        public string Tieude { get; set; }
        public int Thoigianthi { get; set; }
        public string FileDeThi { get; set; }
        public int? SoCau { get; set; }
        public EDeThiType DeThiType { get; set; }
    }
}
