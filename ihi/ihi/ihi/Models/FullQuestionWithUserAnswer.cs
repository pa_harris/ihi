﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ihi.Models
{
    public class FullQuestionWithUserAnswer
    {
        public Proc_GetListCauHoiDeThi_Result CauHois { get; set; }
        public ObservableCollection<Proc_GetListCauHoiDapAnWithUserDeThi_Result> DapAns { get; set; }
    }
}
