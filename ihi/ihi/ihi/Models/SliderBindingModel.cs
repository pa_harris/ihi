﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class SliderBindingModel
    {
        public int QuangcaoId { get; set; }
        public string Tieude { get; set; }
        public string Noidung { get; set; }
        public string ImageURL { get; set; }
        public int? OrderBy { get; set; }
        public ESlideLayout? SlideLayout { get; set; }
        public string LabelPrimary { get; set; }
        public string LabelSecondary { get; set; }
        public string LinkPrimary { get; set; }
        public string LinkSecondary { get; set; }
    }

}
