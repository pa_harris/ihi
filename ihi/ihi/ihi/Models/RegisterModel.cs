﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.Models
{
    public class RegisterModel
    {
        public string ConfirmPassword { get; set; }
        public string Fullname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
