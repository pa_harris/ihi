﻿using ihi.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace ihi.Converters
{
    public class PickerPlaceholderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return parameter.ToString();
            }
            else
            {
                if (value.GetType().Equals(typeof(LopViewModel)))
                {
                    return ((LopViewModel)value).Tieude;
                }
                else if (value.GetType().Equals(typeof(GroupCourseViewModel)))
                {
                    return ((GroupCourseViewModel)value).Tieude;
                }
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
