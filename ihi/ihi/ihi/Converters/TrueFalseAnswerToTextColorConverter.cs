﻿using ihi.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace ihi.Converters
{
    public class TrueFalseAnswerToTextColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                Proc_GetListCauHoiDapAnWithUserDeThi_Result model = value as Proc_GetListCauHoiDapAnWithUserDeThi_Result;
                if (model.Phuongandung == true) // đáp án đúng
                {
                    return "Green";
                }
                else if (model.Traloi.Equals(model.DapancauhoiId) && model.Phuongandung == null) // đáp án sai
                {
                    return "Red";
                }
            }
            return "Black";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public class TrueFalseAnswerToTextColorHtmlConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                Proc_GetListCauHoiDapAnWithUserDeThi_Result model = value as Proc_GetListCauHoiDapAnWithUserDeThi_Result;
                if (model.Phuongandung == true) // đáp án đúng
                {
                    return model.Giatri.Replace("color:black", "color:green");
                }
                else if (model.Traloi.Equals(model.DapancauhoiId) && model.Phuongandung == null) // đáp án sai
                {
                    return model.Giatri.Replace("color:black", "color:red");
                }
                else
                {
                    return model.Giatri;
                }
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
