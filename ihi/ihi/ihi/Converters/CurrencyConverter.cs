﻿using ihi.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace ihi.Converters
{
    public class CurrencyConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                if (value.GetType().ToString().Contains("CourseBindingModel"))
                {
                    CourseBindingModel model = value as CourseBindingModel;
                    if (parameter.ToString().Equals("FinalPrice"))
                    {
                        if (model?.Gia == null)
                        {
                            return "Miễn phí";
                        }
                        else
                        {
                            if (model?.Chietkhau == null)
                            {
                                return ((decimal)model?.Gia).ToString("0,000");
                            }
                            else
                            {
                                return ((decimal)(model?.Gia * (decimal)(model?.Chietkhau / 100))).ToString("0,000");
                            }
                        }
                    }
                    if (parameter.ToString().Equals("Visible"))
                    {
                        if (model?.Gia != null)
                        {
                            if (model?.Chietkhau == null || model?.Chietkhau == 0)
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        return false;
                    }
                }
                else if (value.GetType().ToString().Contains("Decimal"))
                {
                    if (System.Convert.ToInt64(value).ToString().Length < 4)
                    {
                        return System.Convert.ToInt64(value);
                    }
                    else
                    {
                        return System.Convert.ToInt64(value).ToString("0,000");
                    }
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
