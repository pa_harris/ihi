﻿using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaymentPage : ContentPage
    {
        public PaymentPageViewModel ViewModel { get; set; }
        public PaymentPage()
        {
            InitializeComponent();
            BindingContext = ViewModel = new PaymentPageViewModel(this);
        }
    }
}