﻿using ihi.Helpers;
using ihi.UCs;
using Plugin.Media.Abstractions;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PhotoCroppingPage : ContentPage
    {
        PhotoCropperCanvasView photoCropper;
        SKBitmap croppedBitmap;
        MediaFile picture;
        public event Action<MediaFile, SKBitmap> ReturnCropped;

        public PhotoCroppingPage(MediaFile picture, SKBitmap skbitmap)
        {
            InitializeComponent();

            SKBitmap bitmap = skbitmap;
            this.picture = picture;

            photoCropper = new PhotoCropperCanvasView(bitmap);
            canvasViewHost.Children.Add(photoCropper);
        }

        void OnDoneButtonClicked(object sender, EventArgs args)
        {
            actIndicator.IsRunning = true;
            actIndicator.IsEnabled = true;
            actIndicator.IsVisible = true;
            Thread t = new Thread(() =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    croppedBitmap = photoCropper.CroppedBitmap;
                    ReturnCropped(this.picture, croppedBitmap);
                    Application.Current.MainPage.Navigation.PopAsync();
                });
            });
            t.IsBackground = true;
            t.Start();            
        }

        void OnCanvasViewPaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            SKImageInfo info = args.Info;
            SKSurface surface = args.Surface;
            SKCanvas canvas = surface.Canvas;

            canvas.Clear();
            canvas.DrawBitmap(croppedBitmap, info.Rect, BitmapStretch.Uniform);
        }
    }
}