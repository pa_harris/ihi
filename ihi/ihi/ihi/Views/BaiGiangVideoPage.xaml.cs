﻿using ihi.Models;
using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BaiGiangVideoPage : ContentPage
    {
        public BaiGiangVideoPageViewModel ViewModel { get; set; }
        public BaiGiangVideoPage(BaiGiangBindingModel baiGiang = null)
        {
            InitializeComponent();
            BindingContext = ViewModel = new BaiGiangVideoPageViewModel(this, baiGiang);
        }
        protected override void OnSizeAllocated(double width, double height)
        {
            base.OnSizeAllocated(width, height);
            if (width < height)
            {
                // portrait
                ucTopBar.IsVisible = true;
                stkDescription.IsVisible = true;
                videoView.HeightRequest = 300;
                videoView.VerticalOptions = LayoutOptions.Start;
            }
            else
            {
                // landscape
                ucTopBar.IsVisible = false;
                stkDescription.IsVisible = false;
                videoView.VerticalOptions = LayoutOptions.FillAndExpand;
            }
        }
    }
}