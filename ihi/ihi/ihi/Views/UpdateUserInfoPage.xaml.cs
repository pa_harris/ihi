﻿using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UpdateUserInfoPage : ContentPage
    {
        public UpdateUserInfoPageViewModel ViewModel { get; set; }
        public UpdateUserInfoPage()
        {
            InitializeComponent();
            BindingContext = ViewModel = new UpdateUserInfoPageViewModel(this);
        }
    }
}