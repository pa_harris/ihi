﻿using ihi.Models;
using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OTPConfirmationPage : ContentPage
    {
        public OTPConfirmationPageViewModel ViewModel { get; set; }
        public OTPConfirmationPage(RegisterModel registerModel = null)
        {
            InitializeComponent();
            BindingContext = ViewModel = new OTPConfirmationPageViewModel(this, registerModel, null);
        }
        public OTPConfirmationPage(string userName = null)
        {
            InitializeComponent();
            BindingContext = ViewModel = new OTPConfirmationPageViewModel(this, null, userName);
        }
    }
}