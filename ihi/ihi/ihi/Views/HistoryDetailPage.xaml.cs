﻿using ihi.Helpers;
using ihi.Models;
using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HistoryDetailPage : ContentPage
    {
        string googleUrl = "http://drive.google.com/viewerng/viewer?embedded=true&url=";
        public HistoryDetailPageViewModel ViewModel { get; set; }
        public HistoryDetailPage(DeThiHistoryBindingModel model)
        {
            InitializeComponent();
            BindingContext = ViewModel = new HistoryDetailPageViewModel(this, model);
            if (Device.RuntimePlatform == Device.Android)
            {
                model.FileDeThi = $"{googleUrl}{constants.DomainWebsiteUrl}{model.FileDeThi}";
                wvDeThi.Source = new UrlWebViewSource { Url = model.FileDeThi };
            }
            else
            {
                model.FileDeThi = $"{constants.DomainWebsiteUrl}{model.FileDeThi}";
                wvDeThi.Source = new UrlWebViewSource { Url = model.FileDeThi };
            }
        }
    }
}