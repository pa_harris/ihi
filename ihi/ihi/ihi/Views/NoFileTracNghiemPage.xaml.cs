﻿using ihi.Interface;
using ihi.Models;
using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NoFileTracNghiemPage : ContentPage
    {
        public NoFileTracNghiemPageViewModel ViewModel { get; set; }
        public NoFileTracNghiemPage(DeThiBindingModel dethi = null)
        {
            InitializeComponent();
            BindingContext = ViewModel = new NoFileTracNghiemPageViewModel(this, dethi);
        }
    }
}