﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class testwebview : ContentPage
    {
        public testwebview()
        {
            InitializeComponent();
            var Html = @"
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Document</title>
</head>
<body>
<div style='font-size: 10px;'>
    <p style='color: red;'>Xét $f(x)={{x}^{4}}-2(m-1){{x}^{2}}&#43;2m-3\Rightarrow f(x)=0\Leftrightarrow ({{x}^{2}}-1)({{x}^{2}}-2m&#43;3)=0\Leftrightarrow \left[ \begin{gathered}\hfill {{x}^{2}}=1 \\ \hfill {{x}^{2}}=2m-3 \\ \end{gathered} \right..$</p>
    TH1: Nếu $2m-3\le 0\Rightarrow $ Do vậy $f(x)$ có hai điểm đổi dấu $x=-1;x=1.$ Hàm số $y=\left| f(x) \right|$ có 5 điểm cực trị $\Leftrightarrow y=f(x)$ có ba điểm cực trị$\Leftrightarrow ab&lt;0\Leftrightarrow -2(m-1)&lt;0\Leftrightarrow m&gt;1.$
    <p></p>
    Vậy trường hợp này có $1&lt;m\le \dfrac{3}{2}.$
    <p><img src='https://www.vted.vn/upload/ask/images/f862158d-430f-415f-9e83-965a34df13a6\cf5e3f0c.jpg'/> </p>
        TH2: Nếu $0&lt;2m-3\ne 1\Leftrightarrow \dfrac{3}{2}&lt;m\ne 2.$ Khi đó $f(x)$ có bốn điểm đổi dấu $x=\pm 1;x=\pm \sqrt{2m-3}$ do đó số điểm cực trị của hàm số $f(x)$ bằng 3 và hàm số $y=\left| f(x) \right|$ có 7 điểm cực trị (loại).
    <p></p>
    TH3: Nếu $2m-3=1\Leftrightarrow m=2\Rightarrow f(x)={{({{x}^{2}}-1)}^{2}}$ khi đó $y=\left| f(x) \right|={{({{x}^{2}}-1)}^{2}}$ có ba điểm cực trị (loại).
    <p></p>
    <p></p>
    Chọn đáp án D.
    <p></p>
    <p></p>
</div>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-MML-AM_CHTML' async></script>
    <script type='text/x-mathjax-config'>
        document.getElementsByTagName('img')[0].addEventListener('click', function(data){
            window.open(data.toElement.src, '_blank')
        })

        MathJax.Hub.Config({
            showProcessingMessages: false,
            messageStyle: 'none',
            showMathMenu: false,
            tex2jax: {
            inlineMath: [ ['$','$'], ['\\(','\\)'], ['\\[','\\]'] ],
            displayMath: [ ['$$','$$'] ],
            processEscapes: true
            },
            'HTML-CSS': { availableFonts: ['TeX'], linebreaks: { automatic: true, width: '75% container' } },
        });
    </script>
</body>
</html>";
            var html2 = @"
<!DOCTYPE html>
                    <html lang='en'>
                        <head>
                            <meta charset='UTF-8'>
                            <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                            <style>img{ width:100%; height:auto; }</style>
                        </head>
                        <body>
                            <div style='font-size:14px; overflow-y: scroll;'>
                                " + @"  
<p>Cho 0,02mol CO<sub>2</sub> vào 100ml dd Ba(OH)<sub>2</sub> 0,12M và NaOH 0,06M thu được m gam kết tủa. Giá trị của m là</p>

" + @"
                            </div>
                            <script type='text/javascript' async src='MathJax/MathJax.js?config=TeX-MML-AM_CHTML'></script>
                            <script type='text/x-mathjax-config'>
                                 document.querySelectorAll('img').forEach(function(element){
                                    element.addEventListener('click', function(data){
                                         window.open(data.toElement.src, '_blank')
                                     })
                                });

                                MathJax.Hub.Config({
                                    showProcessingMessages: false,
                                    messageStyle: 'none',
                                    showMathMenu: false,
                                    tex2jax: {
                                    inlineMath: [ ['$','$'], ['\\(','\\)'], ['\\[','\\]'] ],
                                    displayMath: [ ['$$','$$'] ],
                                    processEscapes: true
                                    },
                                    'HTML-CSS': { availableFonts: ['TeX'], linebreaks: { automatic: true, width: '75% container' } },
                                });
                            </script>
                        </body>
                        </html>
                    ";
            wv.Source = new HtmlWebViewSource
            {
                Html = html2
            };
        }

        public void WebView_Navigating(object sender, WebNavigatingEventArgs args)
        {
            if (args.Url.StartsWith("file://"))
            {
                return;
            }

            Browser.OpenAsync(new Uri(args.Url), BrowserLaunchMode.SystemPreferred);

            //args.Cancel = true;
        }
    }
}