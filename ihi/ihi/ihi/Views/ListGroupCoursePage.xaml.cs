﻿using ihi.Models;
using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListGroupCoursePage : ContentPage
    {
        public ListGroupCoursePageViewModel ViewModel { get; set; }
        public ListGroupCoursePage()
        {
            InitializeComponent();
            BindingContext = ViewModel = new ListGroupCoursePageViewModel(this);
        }

        public ListGroupCoursePage(string lopId, int? parentGroupCourseId)
        {
            InitializeComponent();
            BindingContext = ViewModel = new ListGroupCoursePageViewModel(this, lopId, parentGroupCourseId);
        }
    }
}