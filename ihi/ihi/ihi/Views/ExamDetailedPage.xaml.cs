﻿using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ExamDetailedPage : ContentPage
    {
        public ExamDetailedPageViewModel ViewModel { get; set; }
        public ExamDetailedPage()
        {
            InitializeComponent();
            BindingContext = ViewModel = new ExamDetailedPageViewModel(this);
        }
    }
}