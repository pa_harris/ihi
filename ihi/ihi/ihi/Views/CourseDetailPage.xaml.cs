﻿using ihi.Models;
using ihi.ViewModels;
using MediaManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CourseDetailPage : ContentPage
    {
        public CourseDetailPageViewModel ViewModel { get; set; }
        public CourseDetailPage(CourseBindingModel model = null, bool isBought = false)
        {
            InitializeComponent();
            //Device.BeginInvokeOnMainThread(() =>
            //{
            //    mediaElement.Source = new Uri(model?.UrlVideo);
            //});
            //Thread t = new Thread(() =>
            //{
            //    mediaElement.Source = new Uri(model?.UrlVideo);
            //});
            //t.Start();
            //mediaElement.Source = new Uri(model?.UrlVideo);
            BindingContext = ViewModel = new CourseDetailPageViewModel(this, model, isBought);
            //CrossMediaManager.Current.Play(vvUrlVideo.Source);
        }
    }
}