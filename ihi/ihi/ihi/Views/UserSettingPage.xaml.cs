﻿using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserSettingPage : ContentPage
    {
        public UserSettingPageViewModel ViewModel { get; set; }
        public UserSettingPage()
        {
            InitializeComponent();
            BindingContext = ViewModel = new UserSettingPageViewModel(this);
        }
    }
}