﻿using ihi.Models;
using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListBaiGiangPage : ContentPage
    {
        public ListBaiGiangPageViewModel ViewModel { get; set; }
        public ListBaiGiangPage(ChuongBindingModel model = null, bool isBought = false)
        {
            InitializeComponent();
            BindingContext = ViewModel = new ListBaiGiangPageViewModel(this, model, isBought);
        }
    }
}