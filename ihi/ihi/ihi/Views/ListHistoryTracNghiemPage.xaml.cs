﻿using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListHistoryTracNghiemPage : ContentPage
    {
        public ListHistoryTracNghiemPageViewModel ViewModel { get; set; }
        public ListHistoryTracNghiemPage()
        {
            InitializeComponent();
            BindingContext = ViewModel = new ListHistoryTracNghiemPageViewModel(this);
        }
    }
}