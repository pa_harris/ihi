﻿using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CarouselPage : ContentPage
    {
        public class CarouselPageModel
        {
            public int Index { get; set; }
            public string Image { get; set; }
            public string Content { get; set; }
        }
        public CarouselPageViewModel ViewModel { get; set; }
        public CarouselPage()
        {
            InitializeComponent();
            BindingContext = ViewModel = new CarouselPageViewModel(this);
        }
    }
}