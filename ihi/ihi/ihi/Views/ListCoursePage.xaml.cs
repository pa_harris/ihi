﻿using ihi.Models;
using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListCoursePage : ContentPage
    {
        public ListCoursePageViewModel ViewModel { get; set; }
        public ListCoursePage(GroupCourseViewModel groupCourse = null)
        {
            InitializeComponent();
            BindingContext = ViewModel = new ListCoursePageViewModel(this, groupCourse);
        }
    }
}