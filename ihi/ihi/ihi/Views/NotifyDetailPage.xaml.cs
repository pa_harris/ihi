﻿using ihi.Models;
using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotifyDetailPage : ContentPage
    {
        public NotifyDetailPageViewModel ViewModel { get; set; }
        public NotifyDetailPage()
        {
            InitializeComponent();
            BindingContext = ViewModel = new NotifyDetailPageViewModel(this);
        }
        public NotifyDetailPage(Proc_GetNotify_Result model)
        {
            InitializeComponent();
            BindingContext = ViewModel = new NotifyDetailPageViewModel(this, model);
        }
    }
}