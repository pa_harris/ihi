﻿using ihi.ViewModels;
using Rg.Plugins.Popup.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views.Popups
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConfirmBuyCoursePopup : PopupPage
    {
        public ICommand OnClickBuy { get; set; }
        public ConfirmBuyCoursePopup(BaseViewModel parentViewModel)
        {
            InitializeComponent();
            if (parentViewModel != null)
            {
                BindingContext = parentViewModel;
            }
            else
            {
                BindingContext = this;
            }
            OnClickBuy = new Command(() =>
            {
                Application.Current.MainPage.DisplayAlert("Thông báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau!", "OK");
            });
        }
    }
}