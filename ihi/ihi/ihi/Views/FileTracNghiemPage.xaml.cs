﻿using ihi.Helpers;
using ihi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FileTracNghiemPage : ContentPage
    {
        string googleUrl = "http://drive.google.com/viewerng/viewer?embedded=true&url=";
        public FileTracNghiemPageViewModel ViewModel { get; set; }
        public FileTracNghiemPage(DeThiBindingModel dethi = null)
        {
            InitializeComponent();
            BindingContext = ViewModel = new FileTracNghiemPageViewModel(this, dethi);
            if (Device.RuntimePlatform == Device.Android)
            {
                dethi.FileDeThi = $"{googleUrl}{constants.DomainWebsiteUrl}{dethi.FileDeThi}";
                wvDeThi.Source = new UrlWebViewSource { Url = dethi.FileDeThi };
            }
            else
            {
                dethi.FileDeThi = $"{constants.DomainWebsiteUrl}{dethi.FileDeThi}";
                wvDeThi.Source = new UrlWebViewSource { Url = dethi.FileDeThi };
            }
        }
    }
}