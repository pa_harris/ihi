﻿using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ForgotPasswordPage : ContentPage
    {
        public ForgotPasswordPageViewModel ViewModel { get; set; }
        public ForgotPasswordPage(string userName = null)
        {
            InitializeComponent();
            BindingContext = ViewModel = new ForgotPasswordPageViewModel(this, userName);
        }
    }
}