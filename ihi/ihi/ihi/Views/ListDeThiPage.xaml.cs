﻿using ihi.Models;
using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ihi.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListDeThiPage : ContentPage
    {
        public ListDeThiPageViewModel ViewModel { get; set; }
        public ListDeThiPage(EDeThiType deThiType = EDeThiType.OnTap)
        {
            InitializeComponent();
            this.BindingContext = ViewModel = new ListDeThiPageViewModel(this, deThiType);
        }
    }
}