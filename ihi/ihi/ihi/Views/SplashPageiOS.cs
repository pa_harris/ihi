﻿using ihi.Helpers;
using ihi.Utilities;
using ihi.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace ihi.Views
{
    public class SplashPageiOS : ContentPage
    {
        public SplashPageiOS()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            Content = new StackLayout
            {
                BackgroundColor = Color.White,
                VerticalOptions = LayoutOptions.FillAndExpand,
                Padding = 0,
                Margin = 0,
                Children = {
                    new Image 
                    {
                        Source = "ihi_logowithtext.png", 
                        HorizontalOptions = LayoutOptions.CenterAndExpand,
                        VerticalOptions = LayoutOptions.CenterAndExpand, 
                        Aspect = Aspect.AspectFit 
                    }
                }
            };
            Device.BeginInvokeOnMainThread(() =>
            {
                LoadUserDataAsync();
            });
        }

        private async void LoadUserDataAsync()
        {
            if (!string.IsNullOrEmpty(constants.AccessToken))
            {
                var user = await AccountUtilities.UserInfo((p) => { });
                if (user != null)
                {
                    //await Application.Current.MainPage.Navigation.PushAsync(new HomePage());
                    await Application.Current.MainPage.Navigation.PushAsync(new CarouselPage());
                }
                else
                {
                    await Application.Current.MainPage.Navigation.PushAsync(new Views.CarouselPage());
                }
                NavigationHelper.RemoveAllPageBefore();
            }
            else
            {
                await Application.Current.MainPage.Navigation.PushAsync(new Views.CarouselPage());
                NavigationHelper.RemoveAllPageBefore();
            }
        }
    }
}