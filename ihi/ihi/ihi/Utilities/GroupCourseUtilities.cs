﻿using ihi.Helpers;
using ihi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ihi.Utilities
{
    public static class GroupCourseUtilities
    {
        public async static Task<List<GroupCourseViewModel>> GetListGroupCourse(string lopId, bool isGetAll, int? parentId, int page, int pageSize, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetListGroupCourse}?lopId={lopId}&isGetAll={isGetAll}&parentId={parentId}&page={page}&pageSize={pageSize}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<List<GroupCourseViewModel>>(responeContent);
                        foreach (var item in respone)
                        {
                            if (item == null)
                            {
                                respone.Remove(item);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(item?.UrlImg))
                                {
                                    item.UrlImg = constants.DomainImageUrl + item.UrlImg;
                                }
                                item.Tieude = item.Title;
                            }
                        }
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                }
            }

            return null;
        }
    }
}
