﻿using ihi.Helpers;
using ihi.Models;
using Newtonsoft.Json;
using Plugin.FacebookClient;
using Plugin.GoogleClient;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ihi.Utilities
{
    public static class SocialUtilities
    {
        public async static Task<List<Proc_GetCapHoc_Result>> GetCapHoc(int? id, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetCapHoc}?id={id}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        if (responeContent.Contains('['))
                        {
                            var respone = JsonConvert.DeserializeObject<List<Proc_GetCapHoc_Result>>(responeContent);
                            return respone;
                        }
                        else
                        {
                            var respone = JsonConvert.DeserializeObject<Proc_GetCapHoc_Result>(responeContent);
                            var result = new List<Proc_GetCapHoc_Result>();
                            result.Add(respone);
                            return result;
                        }
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return null;
        }
        public async static Task<List<Proc_GetKhoi_Result>> GetKhoi(int? id, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetKhoi}?id={id}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        if (responeContent.Contains('['))
                        {
                            var respone = JsonConvert.DeserializeObject<List<Proc_GetKhoi_Result>>(responeContent);
                            return respone;
                        }
                        else
                        {
                            var respone = JsonConvert.DeserializeObject<Proc_GetKhoi_Result>(responeContent);
                            var result = new List<Proc_GetKhoi_Result>();
                            result.Add(respone);
                            return result;
                        }
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return null;
        }
        public async static Task<List<Proc_GetQuanHuyen_Result>> GetQuanHuyen(int? idTinhThanh, int? id, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetQuanHuyen}?idTinhThanh={idTinhThanh}&id={id}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        if (responeContent.Contains('['))
                        {
                            var respone = JsonConvert.DeserializeObject<List<Proc_GetQuanHuyen_Result>>(responeContent);
                            return respone;
                        }
                        else
                        {
                            var respone = JsonConvert.DeserializeObject<Proc_GetQuanHuyen_Result>(responeContent);
                            var result = new List<Proc_GetQuanHuyen_Result>();
                            result.Add(respone);
                            return result;
                        }
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return null;
        }
        public async static Task<List<Proc_GetTinhThanh_Result>> GetTinhThanh(int? id, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetTinhThanh}?id={id}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        if (responeContent.Contains('['))
                        {
                            var respone = JsonConvert.DeserializeObject<List<Proc_GetTinhThanh_Result>>(responeContent);
                            return respone;
                        }
                        else
                        {
                            var respone = JsonConvert.DeserializeObject<Proc_GetTinhThanh_Result>(responeContent);
                            var result = new List<Proc_GetTinhThanh_Result>();
                            result.Add(respone);
                            return result;
                        }
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return null;
        }
        public async static Task<List<Proc_GetTruong_Result>> GetTruong(int? idCapHoc, int? idQuanHuyen, int? id, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetTruong}?idCapHoc={idCapHoc}&idQuanHuyen={idQuanHuyen}&id={id}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        if (responeContent.Contains('['))
                        {
                            var respone = JsonConvert.DeserializeObject<List<Proc_GetTruong_Result>>(responeContent);
                            return respone;
                        }
                        else
                        {
                            var respone = JsonConvert.DeserializeObject<Proc_GetTruong_Result>(responeContent);
                            var result = new List<Proc_GetTruong_Result>();
                            result.Add(respone);
                            return result;
                        }
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return null;
        }
    }
}

