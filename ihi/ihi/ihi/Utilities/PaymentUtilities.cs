﻿using ihi.Helpers;
using ihi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ihi.Utilities
{
    public static class PaymentUtilities
    {
        public static string GetMomoRawHash(MomoRequestModel info)
        {

            string request = "";

            request += "partnerCode=" + constants.partnerCode;
            request += "&accessKey=" + constants.accessKey;
            request += "&requestId=" + info.requestId;
            request += "&amount=" + info.amount;
            request += "&orderId=" + info.orderId;
            request += "&orderInfo=" + info.orderInfo;
            request += "&returnUrl=" + info.returnUrl;
            request += "&notifyUrl=" + info.notifyUrl;
            request += "&extraData=" + info.extraData;

            return request;
        }
        public async static Task<MomoResponeModel> RequestMomoFunction(MomoRequestModel data, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                //http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Cons.AccessToken);
                //http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");
                try
                {
                    string jsonData = JsonConvert.SerializeObject(data);
                    StringContent stringContent = new StringContent(jsonData, Encoding.UTF8, "application/json");
                    var response = await http.PostAsync(constants.MomoTransactionProcessor, stringContent);
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responseContent = await response.Content.ReadAsStringAsync();
                        var responeMomo = JsonConvert.DeserializeObject<MomoResponeModel>(responseContent);
                        if (responeMomo.errorCode != 0)
                        {
                            callback(responeMomo.localMessage);
                            return null;
                        }
                        return responeMomo;
                    }
                    else
                    {
                        var responseContent = await response.Content.ReadAsStringAsync();
                        var responeMomo = JsonConvert.DeserializeObject<MomoResponeModel>(responseContent);
                        callback(responeMomo.localMessage);
                        return null;
                    }
                }
                catch (Exception ee)
                {

                }
            }
            return null;
        }
    }
}
