﻿using ihi.Helpers;
using ihi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ihi.Utilities
{
    public static class TracNghiemUtilities
    {
        public async static Task<List<DeThiBindingModel>> GetListDeThi(GetListDeThiBindingModel model, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    string dataJson = JsonConvert.SerializeObject(model);
                    StringContent stringContent = new StringContent(dataJson, Encoding.UTF8, "application/json");
                    var response = await http.PostAsync(constants.ApiGetListDeThi, stringContent);


                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<List<DeThiBindingModel>>(responeContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return null;
        }
        public async static Task<DeThiDetailBindingModel> GetDeThiDetail(string ontapId, string dethiId, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetDeThiDetail}?ontapId={ontapId}&dethiId={dethiId}");


                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        responeContent = responeContent.Replace("\\r\\n", "");
                        var respone = JsonConvert.DeserializeObject<DeThiDetailBindingModel>(responeContent);
                        foreach (var question in respone.CauHois)
                        {
                            question.Noidung = TemplateHtmlHelper.DisplayHtmlContent(question.Noidung);
                        }
                        foreach (var answer in respone.DapAns)
                        {
                            answer.Giatri = TemplateHtmlHelper.DisplayHtmlContent(answer.Giatri);
                        }
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return null;
        }
        public async static Task<double> SubmitAnswer(string ontapId, string dethiId, List<DapAnSubmitModel> listDapAn, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    string dataJson = JsonConvert.SerializeObject(listDapAn);
                    StringContent stringContent = new StringContent(dataJson, Encoding.UTF8, "application/json");
                    var response = await http.PostAsync($"{constants.ApiSubmitAnswer}?ontapId={ontapId}&dethiId={dethiId}", stringContent);


                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<double>(responeContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return -1;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return -1;
        }
        public async static Task<List<DeThiHistoryBindingModel>> GetListHistory(Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetListHistory}");
                    
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<List<DeThiHistoryBindingModel>>(responeContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return null;
        }
        public async static Task<DeThiHistoryDetailBindingModel> GetHistoryDetail(string hoctapId, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetHistoryDetail}?hoctapId={hoctapId}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        responeContent = responeContent.Replace("\\r\\n", "");
                        var respone = JsonConvert.DeserializeObject<DeThiHistoryDetailBindingModel>(responeContent);
                        foreach (var question in respone.CauHois)
                        {
                            question.Noidung = TemplateHtmlHelper.DisplayHtmlContent(question.Noidung);
                        }
                        foreach (var answer in respone.DapAns)
                        {
                            answer.Giatri = TemplateHtmlHelper.DisplayHtmlContent(answer.Giatri);
                        }
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return null;
        }
    }
}
