﻿using ihi.Helpers;
using ihi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ihi.Utilities
{
    public static class UserUtilities
    {
        public async static Task<List<ProfileTeacherViewModel>> GetListTeacher(int? page, int? pageSize, Action<string> callback, string uid = null)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetListTeacher}?uid={uid}&page={page}&pageSize={pageSize}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<List<ProfileTeacherViewModel>>(responeContent);
                        foreach (var item in respone)
                        {
                            if (!string.IsNullOrEmpty(item?.Avatar))
                            {
                                item.Avatar = constants.DomainImageUrl + item.Avatar;
                            }
                        }
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                }
            }

            return null;
        }
        public async static Task<List<Proc_GetListModPagination_Result>> GetListMod(int? page, int? pageSize, Action<string> callback, string uid = null)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetListMod}?uid={uid}&page={page}&pageSize={pageSize}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<List<Proc_GetListModPagination_Result>>(responeContent);
                        foreach (var item in respone)
                        {
                            if (!string.IsNullOrEmpty(item?.Avatar))
                            {
                                item.Avatar = constants.DomainImageUrl + item.Avatar;
                            }
                        }
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                }
            }

            return null;
        }
    }
}
