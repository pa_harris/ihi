﻿using ihi.Helpers;
using ihi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ihi.Utilities
{
    public static class PushNotificationUtilities
    {
        public async static Task<bool> SaveFcmDeviceToken(SaveFcmDeviceTokenViewModel data, Action<string> callback)
        {
            if (string.IsNullOrEmpty(constants.AccessToken))
            {
                callback("Vui lòng thoát ra và đăng nhập lại!");
                return false;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    string dataJson = JsonConvert.SerializeObject(data);
                    StringContent stringContent = new StringContent(dataJson, Encoding.UTF8, "application/json");
                    var response = await http.PostAsync(constants.ApiSaveFcmDeviceToken, stringContent);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        constants.FirebaseToken = data.NewToken;
                        return true;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return false;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return false;
        }
        public async static Task<bool> RemoveFcmDeviceToken(RemoveFcmDeviceTokenViewModel data, Action<string> callback)
        {
            if (string.IsNullOrEmpty(constants.AccessToken))
            {
                callback("Vui lòng thoát ra và đăng nhập lại!");
                return false;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    string dataJson = JsonConvert.SerializeObject(data);
                    StringContent stringContent = new StringContent(dataJson, Encoding.UTF8, "application/json");
                    var response = await http.PostAsync(constants.ApiRemoveFcmDeviceToken, stringContent);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        return true;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return false;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return false;
        }
    }
}
