﻿using ihi.Helpers;
using ihi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ihi.Utilities
{
    public static class CommonUtilities
    {
        public async static Task<List<SliderBindingModel>> GetHomeSlider(Action<string> callback)
        {
            if (string.IsNullOrEmpty(constants.AccessToken))
            {
                callback("Vui lòng thoát ra và đăng nhập lại!");
                return null;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync(constants.ApiGetHomeSlider);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<List<SliderBindingModel>>(responeContent);

                        if (respone != null)
                        {
                            foreach (var sliderItem in respone)
                            {
                                sliderItem.ImageURL = string.IsNullOrEmpty(sliderItem.ImageURL) ? "" : constants.DomainImageUrl + sliderItem.ImageURL;
                            }
                        }

                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return null;
        }

        public async static Task<List<ClientCommentViewModel>> GetListComment(Action<string> callback)
        {
            if (string.IsNullOrEmpty(constants.AccessToken))
            {
                callback("Vui lòng thoát ra và đăng nhập lại!");
                return null;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync(constants.ApiGetListComment);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<List<ClientCommentViewModel>>(responeContent);

                        if (respone != null)
                        {
                            foreach (var commentItem in respone)
                            {
                                commentItem.Avatar = string.IsNullOrEmpty(commentItem.Avatar) ? "" : constants.DomainImageUrl + commentItem.Avatar;
                            }
                        }

                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return null;
        }
    }
}
