﻿using ihi.Helpers;
using ihi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ihi.Utilities
{
    public static class NotifyUtilities
    {
        public async static Task<int> CountNewest(Action<string> callback)
        {
            if (string.IsNullOrEmpty(constants.AccessToken))
            {
                callback("Vui lòng thoát ra và đăng nhập lại!");
                return 0;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync(constants.ApiCountNewest);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<int>(responeContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return 0;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return 0;
        }
        public async static Task<List<Proc_GetNotify_Result>> GetNotifies(int? page, int? pageSize, Action<string> callback)
        {
            if (string.IsNullOrEmpty(constants.AccessToken))
            {
                callback("Vui lòng thoát ra và đăng nhập lại!");
                return null;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetNotifies}?page={page}&pageSize={pageSize}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<List<Proc_GetNotify_Result>>(responeContent);
                        if (respone != null)
                        {
                            foreach (var item in respone)
                            {
                                item.Avatar = string.IsNullOrEmpty(item.Avatar) ? "" : constants.DomainImageUrl + item.Avatar;
                            }
                        }
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return null;
        }
        public async static Task<Proc_GetNotify_Result> UpdateNotifyIsRead(UpdateNotifyIsReadBindingModel data, Action<string> callback)
        {
            if (string.IsNullOrEmpty(constants.AccessToken))
            {
                callback("Vui lòng thoát ra và đăng nhập lại!");
                return null;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    string dataJson = JsonConvert.SerializeObject(data);
                    StringContent stringContent = new StringContent(dataJson, Encoding.UTF8, "application/json");
                    var response = await http.PostAsync(constants.ApiUpdateNotifyIsRead, stringContent);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<Proc_GetNotify_Result>(responeContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return null;
        }
    }
}
