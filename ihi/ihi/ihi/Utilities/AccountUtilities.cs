﻿using ihi.Helpers;
using ihi.Models;
using Newtonsoft.Json;
using Plugin.FacebookClient;
using Plugin.FirebasePushNotification;
using Plugin.GoogleClient;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ihi.Utilities
{
    public static class AccountUtilities
    {
        public async static Task<bool> Login(AccountLoginModel acc, Action<string> callback)
        {
            if (string.IsNullOrEmpty(acc.UserName) || string.IsNullOrEmpty(acc.Password))
            {
                callback("Tên đăng nhập hoặc password không được rỗng!");
                return false;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer");
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                var formContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", acc.UserName),
                    new KeyValuePair<string, string>("password", acc.Password)
                });

                try
                {
                    var response = await http.PostAsync(constants.DomainAPIUrl + "/token", formContent);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<LoginResponse>(responeContent);
                        if (!string.IsNullOrEmpty(respone.access_token))
                        {
                            constants.AccessToken = respone.access_token;
                            return true;
                        }
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ResponseFailed>(responeContent);
                        callback(respone.error_description);
                        return false;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                    callback("Đã có lỗi xảy ra! Vui lòng quay lại sau!");
                }
            }

            constants.AccessToken = null;
            return false;
        }
        public async static Task<ProfileBasesViewModel> UserInfo(Action<string> callback)
        {
            if (string.IsNullOrEmpty(constants.AccessToken))
            {
                callback("Vui lòng thoát ra và đăng nhập lại!");
                return null;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync(constants.ApiUserInfo);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ProfileBasesViewModel>(responeContent);
                        constants.Account = respone;
                        if (!string.IsNullOrEmpty(constants.Account?.Avatar))
                        {
                            constants.Account.Avatar = constants.DomainImageUrl + constants.Account.Avatar;
                        }
                        
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return null;
        }
        public async static Task<bool> UpdateUserInfo(ProfileBasesViewModel model, Action<string> callback)
        {
            if (string.IsNullOrEmpty(constants.AccessToken))
            {
                callback("Vui lòng thoát ra và đăng nhập lại!");
                return false;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    string dataJson = JsonConvert.SerializeObject(model);
                    StringContent stringContent = new StringContent(dataJson, Encoding.UTF8, "application/json");
                    var response = await http.PostAsync(constants.ApiUpdateUserInfo, stringContent);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<bool>(responeContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return false;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return false;
        }
        public async static Task<bool> ForgotPassword(ForgotPasswordBindingModel model, Action<string> callback)
        {
            if (string.IsNullOrEmpty(model.Email))
            {
                callback("Email không được trống!");
                return false;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer");
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    string dataJson = JsonConvert.SerializeObject(model);
                    StringContent stringContent = new StringContent(dataJson, Encoding.UTF8, "application/json");
                    var response = await http.PostAsync(constants.ApiForgotPassword, stringContent);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<bool>(responeContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return false;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return false;
        }
        public async static Task<bool> ResetPassword(ResetPasswordBindingModel model, Action<string> callback)
        {
            if (string.IsNullOrEmpty(model.Email))
            {
                callback("Email không được trống!");
                return false;
            }
            if (string.IsNullOrEmpty(model.NewPassword))
            {
                callback("Mật khẩu không được trống!");
                return false;
            }
            if (string.IsNullOrEmpty(model.ConfirmPassword))
            {
                callback("Xác nhận mật khẩu không được trống!");
                return false;
            }
            if (string.IsNullOrEmpty(model.OTP))
            {
                callback("OTP không được trống!");
                return false;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer");
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    string dataJson = JsonConvert.SerializeObject(model);
                    StringContent stringContent = new StringContent(dataJson, Encoding.UTF8, "application/json");
                    var response = await http.PostAsync(constants.ApiResetPassword, stringContent);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<bool>(responeContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return false;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return false;
        }
        public async static Task<bool> ChangePassword(ChangePasswordBindingModel model, Action<string> callback)
        {
            if (string.IsNullOrEmpty(model.OldPassword))
            {
                callback("Mật khẩu cũ không được trống!");
                return false;
            }
            if (string.IsNullOrEmpty(model.NewPassword))
            {
                callback("Mật khẩu không được trống!");
                return false;
            }
            if (string.IsNullOrEmpty(model.ConfirmPassword))
            {
                callback("Xác nhận mật khẩu không được trống!");
                return false;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    string dataJson = JsonConvert.SerializeObject(model);
                    StringContent stringContent = new StringContent(dataJson, Encoding.UTF8, "application/json");
                    var response = await http.PostAsync(constants.ApiChangePassword, stringContent);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<bool>(responeContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return false;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return false;
        }
        public async static Task<bool> ResendOTP(ResendOtpModel model, Action<string> callback)
        {
            if (string.IsNullOrEmpty(model.Email))
            {
                callback("Email không được trống!");
                return false;
            }
            if (string.IsNullOrEmpty(model.ActionType.ToString()))
            {
                callback("Hành động không được trống!");
                return false;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    string dataJson = JsonConvert.SerializeObject(model);
                    StringContent stringContent = new StringContent(dataJson, Encoding.UTF8, "application/json");
                    var response = await http.PostAsync(constants.ApiResendOtp, stringContent);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<bool>(responeContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return false;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return false;
        }
        public async static Task<bool> RegisterConfirmOtp(ConfirmOtpModel model, Action<string> callback)
        {
            if (string.IsNullOrEmpty(model.Email))
            {
                callback("Email không được trống!");
                return false;
            }
            if (string.IsNullOrEmpty(model.Otp.ToString()))
            {
                callback("OTP không được trống!");
                return false;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer");
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    string dataJson = JsonConvert.SerializeObject(model);
                    StringContent stringContent = new StringContent(dataJson, Encoding.UTF8, "application/json");
                    var response = await http.PostAsync(constants.ApiRegisterConfirmOtp, stringContent);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<bool>(responeContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return false;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return false;
        }
        public async static Task<bool> Register(RegisterBindingModel model, Action<string> callback)
        {
            if (string.IsNullOrEmpty(model.Email))
            {
                callback("Email không được trống!");
                return false;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    string dataJson = JsonConvert.SerializeObject(model);
                    StringContent stringContent = new StringContent(dataJson, Encoding.UTF8, "application/json");
                    var response = await http.PostAsync(constants.ApiRegister, stringContent);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<bool>(responeContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return false;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return false;
        }
        public async static Task<bool> ExternalLogins(ExternalLoginBindingModel model, Action<string> callback)
        {
            if (string.IsNullOrEmpty(model.LoginProvider))
            {
                callback("Login provider không được trống!");
                return false;
            }
            if (string.IsNullOrEmpty(model.ExternalAccessToken))
            {
                callback("External access token không được trống!");
                return false;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    string dataJson = JsonConvert.SerializeObject(model);
                    StringContent stringContent = new StringContent(dataJson, Encoding.UTF8, "application/json");
                    var response = await http.PostAsync(constants.ApiExternalLogins, stringContent);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<LoginResponse>(responeContent);
                        if (!string.IsNullOrEmpty(respone.access_token))
                        {
                            constants.AccessToken = respone.access_token;
                            return true;
                        }
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        CrossFacebookClient.Current.Logout();
                        CrossGoogleClient.Current.Logout();
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return false;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            constants.AccessToken = null;
            return false;
        }
        public async static Task<string> ChangeAvatar(SKData sKData, Plugin.Media.Abstractions.MediaFile mediaFile, Action<string> callback)
        {
            if (sKData == null || mediaFile == null)
            {
                callback("Đã có lỗi xảy ra! Vui lòng quay lại sau!");
                return null;
            }
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                var content = new MultipartFormDataContent();

                var fileName = mediaFile.Path.Split('\\').LastOrDefault().Split('/').LastOrDefault();
                var fileContent = new StreamContent(sKData.AsStream());
                fileContent.Headers.Add("Content-Type", "image/" + Path.GetExtension(fileName).Replace(".", ""));
                content.Add(fileContent, "\"image\"", fileName);

                try
                {
                    var response = await http.PostAsync(constants.ApiChangeAvatar, content);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var stringContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<string>(stringContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }
            return null;
        }
    }
}

