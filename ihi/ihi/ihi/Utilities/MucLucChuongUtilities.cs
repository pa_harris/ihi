﻿using ihi.Helpers;
using ihi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ihi.Utilities
{
    public static class MucLucChuongUtilities
    {
        public async static Task<List<ChuongBindingModel>> GetMucLucByKhoaHocId(string kid, Action<string> callback)
        {
            if (string.IsNullOrEmpty(kid))
            {
                callback("Vui lòng thoát ra và chọn khóa học!");
                return null;
            }

            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetChuongByKhoaHocId}?kid={kid}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<List<ChuongBindingModel>>(responeContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                    // res = Application.Current.Resources["txtCoLoi"] as string;
                }
            }

            return null;
        }
    }
}
