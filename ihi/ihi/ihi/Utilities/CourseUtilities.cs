﻿using ihi.Helpers;
using ihi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ihi.Utilities
{
    public static class CourseUtilities
    {
        public async static Task<List<CourseBindingModel>> GetBestSellingCourses(string lopId, string monId, int? childGroupId, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetBestSellingCourses}?lopId={lopId}&monId={monId}&childGroupId={childGroupId}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<List<CourseBindingModel>>(responeContent);
                        foreach (var item in respone)
                        {
                            if (!string.IsNullOrEmpty(item?.UrlImg))
                            {
                                item.UrlImg = constants.DomainImageUrl + item.UrlImg;
                            }
                            if (!string.IsNullOrEmpty(item?.UrlVideo))
                            {
                                item.UrlVideo = constants.DomainImageUrl + item.UrlVideo;
                            }
                        }
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                }
            }

            return null;
        }
        public async static Task<List<CourseBindingModel>> GetDiscountCourses(string lopId, string monId, int? childGroupId, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetDiscountCourses}?lopId={lopId}&monId={monId}&childGroupId={childGroupId}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<List<CourseBindingModel>>(responeContent);
                        foreach (var item in respone)
                        {
                            if (!string.IsNullOrEmpty(item?.UrlImg))
                            {
                                item.UrlImg = constants.DomainImageUrl + item.UrlImg;
                            }
                            if (!string.IsNullOrEmpty(item?.UrlVideo))
                            {
                                item.UrlVideo = constants.DomainImageUrl + item.UrlVideo;
                            }
                        }
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                }
            }

            return null;
        }
        public async static Task<List<PopularCourseBindingModel>> GetListPopularGroupCourse(int? page, int? pageSize, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetListPopularGroupCourse}?page={page}&pageSize={pageSize}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<List<PopularCourseBindingModel>>(responeContent);
                        foreach (var item in respone)
                        {
                            if (!string.IsNullOrEmpty(item?.UrlImg))
                            {
                                item.UrlImg = constants.DomainImageUrl + item.UrlImg;
                            }
                        }
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                }
            }

            return null;
        }
        public async static Task<List<CourseBindingModel>> GetListCourse(string gid, int page, int pageSize, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetListCourse}?gid={gid}&page={page}&pageSize={pageSize}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<List<CourseBindingModel>>(responeContent);
                        foreach (var item in respone)
                        {
                            if (!string.IsNullOrEmpty(item?.UrlImg))
                            {
                                item.UrlImg = constants.DomainImageUrl + item.UrlImg;
                            }
                            if (!string.IsNullOrEmpty(item?.UrlVideo))
                            {
                                item.UrlVideo = constants.DomainImageUrl + item.UrlVideo;
                            }
                        }
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                }
            }

            return null;
        }
        public async static Task<List<CourseBindingModel>> GetListBoughtCourse(Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.GetAsync($"{constants.ApiGetListBoughtCourse}");

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<List<CourseBindingModel>>(responeContent);
                        foreach (var item in respone)
                        {
                            if (!string.IsNullOrEmpty(item?.UrlImg))
                            {
                                item.UrlImg = constants.DomainImageUrl + item.UrlImg;
                            }
                            if (!string.IsNullOrEmpty(item?.UrlVideo))
                            {
                                item.UrlVideo = constants.DomainImageUrl + item.UrlVideo;
                            }
                        }
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return null;
                    }
                }
                catch (Exception ee)
                {
                }
            }

            return null;
        }
        public async static Task<bool> BuyCourse(string cid, Action<string> callback)
        {
            using (HttpClient http = new HttpClient())
            {
                http.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", constants.AccessToken);
                http.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "application/json");

                try
                {
                    var response = await http.PostAsync($"{constants.ApiBuyCourse}?cid={cid}", null);

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<bool>(responeContent);
                        return respone;
                    }
                    else
                    {
                        var responeContent = await response.Content.ReadAsStringAsync();
                        var respone = JsonConvert.DeserializeObject<ErrorRespone>(responeContent);
                        callback(ModelStateError.DesearializeModelStateError(respone));
                        return false;
                    }
                }
                catch (Exception ee)
                {
                }
            }

            return false;
        }
    }
}
