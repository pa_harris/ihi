﻿using ihi.Helpers;
using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using Plugin.FirebasePushNotification;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;
using Xamarin.Forms.Xaml;

namespace ihi
{
    public partial class App : Xamarin.Forms.Application
    {
        public App()
        {
            Xamarin.Forms.Application.Current.On<Xamarin.Forms.PlatformConfiguration.Android>().UseWindowSoftInputModeAdjust(WindowSoftInputModeAdjust.Resize);
            Device.SetFlags(new string[] { "MediaElement_Experimental" });
            InitializeComponent();
            if (Device.RuntimePlatform == Device.iOS)
            {
                MainPage = new NavigationPage(new SplashPageiOS());
                //MainPage = new SplashPageiOS();
            }
            else if (Device.RuntimePlatform == Device.Android)
            {
                if (constants.Account != null)
                {
                    MainPage = new NavigationPage(new TabMainPage());
                }
                else
                {
                    MainPage = new NavigationPage(new Views.CarouselPage());
                }
            }
        }

        protected override void OnAppLinkRequestReceived(Uri uri)
        {

        }

        protected override void OnStart()
        {
            SettingFirebasePushNotification();
            UpdateFCMToken();
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        public static void SettingFirebasePushNotification()
        {
            CrossFirebasePushNotification.Current.RegisterForPushNotifications();
            CrossFirebasePushNotification.Current.Subscribe(new string[] { "general", "na2" });
            CrossFirebasePushNotification.Current.OnTokenRefresh += async (s, p) =>
            {
                System.Diagnostics.Debug.WriteLine($"TOKEN : {p.Token}");
                UpdateFCMToken();
            };

            CrossFirebasePushNotification.Current.OnNotificationReceived += async (s, p) =>
            {
                // event vào khi đang mở app và có thông báo
                string title = p.Data["title"].ToString();
                string body = p.Data["body"].ToString();
                Device.BeginInvokeOnMainThread(() =>
                {
                    Xamarin.Forms.Application.Current.MainPage.DisplayAlert("Thông báo", body, "OK");
                });
            };

            CrossFirebasePushNotification.Current.OnNotificationAction += async (s, p) =>
            {
                
            };

            CrossFirebasePushNotification.Current.OnNotificationOpened += async (s, p) =>
            {
                // event vào khi đang tắt app và có thông báo
                string title = p.Data["title"].ToString();
                string body = p.Data["body"].ToString();

                TabMainPage tabMainPage = new TabMainPage();
                tabMainPage.CurrentPage = tabMainPage.Children[3];
                await Current.MainPage.Navigation.PushAsync(tabMainPage);
                await Xamarin.Forms.Application.Current.MainPage.DisplayAlert("Thông báo", body, "OK");
            };
        }

        public static async void UpdateFCMToken()
        {
            if (!string.IsNullOrEmpty(constants.AccessToken))
            {
                if (!CrossFirebasePushNotification.Current.Token.Equals(constants.AccessToken))
                {
                    SaveFcmDeviceTokenViewModel saveModel = new SaveFcmDeviceTokenViewModel
                    {
                        NewToken = CrossFirebasePushNotification.Current.Token,
                        OldToken = constants.FirebaseToken,
                        Platform = (Device.RuntimePlatform == Device.iOS) ? "ios" : "android"
                    };

                    bool isSaveNew = await PushNotificationUtilities.SaveFcmDeviceToken(saveModel, (o) =>
                    {
                    });
                }
            }
        }
    }
}
