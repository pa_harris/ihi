﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ihi.Helpers
{
    public static class NavigationHelper
    {
        public static void RemoveAllPageBefore()
        {
            int count = Xamarin.Forms.Application.Current.MainPage.Navigation.NavigationStack.Count;
            for (int i = 0; i < count - 1; i++)
            {
                Xamarin.Forms.Application.Current.MainPage.Navigation.RemovePage(Xamarin.Forms.Application.Current.MainPage.Navigation.NavigationStack[0]);
            }
        }
    }
    public static class CollectionHelper
    {
        public static void AddList<T>(ObservableCollection<T> des, List<T> source)
        {
            if (source != null)
            {
                foreach (var item in source)
                {
                    des.Add(item);
                }
            }
        }
    }
    public static class CryptographyHelper
    {
        public static string signSHA256(string message, string key)
        {
            byte[] keyByte = Encoding.UTF8.GetBytes(key);
            byte[] messageBytes = Encoding.UTF8.GetBytes(message);
            using (var hmacsha256 = new HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                string hex = BitConverter.ToString(hashmessage);
                hex = hex.Replace("-", "").ToLower();
                return hex;
            }
        }
    }
    public static class RandomGenerator
    {
        public static Random random;
        public static int RandomNumber(int min, int max)
        {
            if (random == null)
            {
                random = new Random();
            }
            return random.Next(min, max);
        }
        public static string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            if (random == null)
            {
                random = new Random();
            }
            char ch;
            for (int i = 0; i < size; i++)
            {
                int isIntNumeric = RandomNumber(0, 2);
                bool isNumeric = (isIntNumeric == 0) ? false : true;
                if (isNumeric)
                {
                    ch = Convert.ToChar(Convert.ToInt32(Math.Floor(10 * random.NextDouble() + 48)));
                    while (builder.ToString().Contains(ch))
                    {
                        ch = Convert.ToChar(Convert.ToInt32(Math.Floor(10 * random.NextDouble() + 48)));
                    }
                }
                else
                {
                    ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                    while (builder.ToString().Contains(ch))
                    {
                        ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                    }
                }

                builder.Append(ch);
            }
            if (lowerCase)
            {
                return builder.ToString().ToLower();
            }
            return builder.ToString();
        }
    }
    public static class PropertyCopier<TParent, TChild> where TParent : class
                                            where TChild : class
    {
        public static void Copy(TParent parent, TChild child)
        {
            if (parent != null && child != null)
            {
                var parentProperties = parent.GetType().GetProperties();
                var childProperties = child.GetType().GetProperties();

                foreach (var parentProperty in parentProperties)
                {
                    var isvirtualParent = parentProperty.GetMethod.IsVirtual;
                    if (isvirtualParent)
                        continue;
                    foreach (var childProperty in childProperties)
                    {
                        var isvirtualchildren = childProperty.GetMethod.IsVirtual;
                        if (isvirtualchildren)
                            continue;
                        if (parentProperty.Name == childProperty.Name && parentProperty.PropertyType == childProperty.PropertyType)
                        {
                            try
                            {
                                childProperty.SetValue(child, parentProperty.GetValue(parent));
                            }
                            catch { }
                            break;
                        }
                    }
                }
            }
            else
            {
                return;
            }
        }
    }

    public static class TemplateHtmlHelper
    {
        public static string DisplayHtmlContent(string body, string color = "black")
        {
            var html = @"
                    <!DOCTYPE html>
                    <html lang='vi'>
                        <head>
                            <meta charset='UTF-8'>
                            <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                            <style>img{ width:100%; height:auto; }</style>
                        </head>
                        <body>
                            <div style='font-size:14px; color:" + color + @"; overflow-y: scroll;'>
                                " + body.Replace("\n", "").Replace("\r", "") + @"
                            </div>
                            <script type='text/javascript' async src='MathJax/MathJax.js?config=TeX-MML-AM_CHTML'></script>
                            <script type='text/x-mathjax-config'>
                                 document.querySelectorAll('img').forEach(function(element){
                                    element.addEventListener('click', function(data){
                                         window.open(data.toElement.src, '_blank')
                                     })
                                });

                                MathJax.Hub.Config({
                                    showProcessingMessages: false,
                                    messageStyle: 'none',
                                    showMathMenu: false,
                                    tex2jax: {
                                    inlineMath: [ ['$','$'], ['\\(','\\)'], ['\\[','\\]'] ],
                                    displayMath: [ ['$$','$$'] ],
                                    processEscapes: true
                                    },
                                    'HTML-CSS': { availableFonts: ['TeX'], linebreaks: { automatic: true, width: '75% container' } },
                                });
                            </script>
                        </body>
                        </html>
                    ";
            return html;
        }
    }
}
