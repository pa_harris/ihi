﻿using ihi.Models;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace ihi.Helpers
{
    public static class constants
    {
#if DEBUG
        //public static string DomainAPIUrl = "http://360997e83235.ngrok.io";
        public static string DomainAPIUrl = "http://api.ihi.vn";
#else
        public static string DomainAPIUrl = "http://api.ihi.vn";
#endif

        //public static string DomainImageUrl = DomainAPIUrl;
        public static string DomainImageUrl = "http://ihi.vn";
        public static string DomainWebsiteUrl = "http://ihi.vn";

        public static string AccessToken { get => Preferences.Get("accessTokenIhi", ""); set { Preferences.Set("accessTokenIhi", value); } }
        public static string FirebaseToken { get => Preferences.Get("firebaseTokenIhi", ""); set { Preferences.Set("firebaseTokenIhi", value); } }
        public static string MailSupport = "support@ihi.vn";
        public static string PhoneSupport = "0909 256 437";

        public static ProfileBasesViewModel Account { get; set; }

        #region Link API
        public static string ApiUserInfo = $"{DomainAPIUrl}/api/Account/UserInfo";
        public static string ApiUpdateUserInfo = $"{DomainAPIUrl}/api/Account/UpdateUserInfo";
        public static string ApiForgotPassword = $"{DomainAPIUrl}/api/Account/ForgotPassword";
        public static string ApiResetPassword = $"{DomainAPIUrl}/api/Account/ResetPassword";
        public static string ApiChangePassword = $"{DomainAPIUrl}/api/Account/ChangePassword";
        public static string ApiResendOtp = $"{DomainAPIUrl}/api/Account/ResendOtp";
        public static string ApiRegister = $"{DomainAPIUrl}/api/Account/Register";
        public static string ApiRegisterConfirmOtp = $"{DomainAPIUrl}/api/Account/RegisterConfirmOtp";
        public static string ApiExternalLogins = $"{DomainAPIUrl}/api/Account/ExternalLogins";
        public static string ApiGetBestSellingCourses = $"{DomainAPIUrl}/api/Course/GetBestSellingCourses";
        public static string ApiGetDiscountCourses = $"{DomainAPIUrl}/api/Course/GetDiscountCourses";
        public static string ApiGetListTeacher = $"{DomainAPIUrl}/api/User/GetListTeacher";
        public static string ApiGetListMod = $"{DomainAPIUrl}/api/User/GetListMod";
        public static string ApiGetListPopularGroupCourse = $"{DomainAPIUrl}/api/Course/GetListPopularGroupCourse";
        public static string ApiGetListVoucher = $"{DomainAPIUrl}/api/Voucher/GetListVoucher";
        public static string ApiGetListLop = $"{DomainAPIUrl}/api/Lop/GetListLop";
        public static string ApiGetListMon = $"{DomainAPIUrl}/api/Mon/GetListMon";
        public static string ApiGetListGroupCourse = $"{DomainAPIUrl}/api/GroupCourse/GetListGroupCourse";
        public static string ApiGetListCourse = $"{DomainAPIUrl}/api/Course/GetListCourse";
        public static string ApiGetChuongByKhoaHocId = $"{DomainAPIUrl}/api/MucLucChuong/GetChuongByKhoaHocId";
        public static string ApiGetBaiGiangByMucLucId = $"{DomainAPIUrl}/api/BaiGiang/GetBaiGiangByMucLucId";
        public static string ApiBuyCourse = $"{DomainAPIUrl}/api/Course/BuyCourse";
        public static string ApiChangeAvatar = $"{DomainAPIUrl}/api/Account/ChangeAvatar";
        public static string ApiGetListBoughtCourse = $"{DomainAPIUrl}/api/Course/GetListBoughtCourse";
        public static string ApiGetBaiGiangDetailById = $"{DomainAPIUrl}/api/BaiGiang/GetBaiGiangDetailById";
        public static string ApiGetListComment = $"{DomainAPIUrl}/api/Common/GetListComment";
        
        public static string ApiGetCapHoc = $"{DomainAPIUrl}/api/Social/GetCapHoc";
        public static string ApiGetKhoi = $"{DomainAPIUrl}/api/Social/GetKhoi";
        public static string ApiGetQuanHuyen = $"{DomainAPIUrl}/api/Social/GetQuanHuyen";
        public static string ApiGetTinhThanh = $"{DomainAPIUrl}/api/Social/GetTinhThanh";
        public static string ApiGetTruong = $"{DomainAPIUrl}/api/Social/GetTruong";

        public static string ApiGetListDeThi = $"{DomainAPIUrl}/api/TracNghiem/GetListDeThi";
        public static string ApiGetDeThiDetail = $"{DomainAPIUrl}/api/TracNghiem/GetDeThiDetail";
        public static string ApiSubmitAnswer = $"{DomainAPIUrl}/api/TracNghiem/SubmitAnswer";
        public static string ApiGetListHistory = $"{DomainAPIUrl}/api/TracNghiem/GetListHistory";
        public static string ApiGetHistoryDetail = $"{DomainAPIUrl}/api/TracNghiem/GetHistoryDetail";

        public static string ApiGetHomeSlider = $"{DomainAPIUrl}/api/Common/GetHomeSlider";

        public static string ApiSaveFcmDeviceToken = $"{DomainAPIUrl}/api/PushNotification/SaveFcmDeviceToken";
        public static string ApiRemoveFcmDeviceToken = $"{DomainAPIUrl}/api/PushNotification/RemoveFcmDeviceToken";

        public static string ApiCountNewest = $"{DomainAPIUrl}/api/Notify/CountNewest";
        public static string ApiGetNotifies = $"{DomainAPIUrl}/api/Notify/GetNotifies";
        public static string ApiUpdateNotifyIsRead = $"{DomainAPIUrl}/api/Notify/UpdateNotifyIsRead";
        #endregion

        #region Momo
        public static string MomoReturnUrl = "ihi://ihi.vn/responemomo";
        //public static string MomoNotifyUrl = "https://test-payment.momo.vn/gw_payment/transactionProcessor";
        public static string MomoNotifyUrl = $"{DomainAPIUrl}/payment/Momo_IPN";
        public static string MomoTransactionProcessor = "https://test-payment.momo.vn/gw_payment/transactionProcessor";
        //public static string MomoTransactionProcessor = "https://payment.momo.vn/gw_payment/transactionProcessor";
        public static string partnerCode = "MOMO4OXY20200305";
        public static string accessKey = "FkKkosPsleIjHq3j";
        public static string secretKey = "hpCLk9LtOGQCwbeHg4U3r1KXWfuMiwL0";
        public static string publicKey = "";
        #endregion
    }
}
