﻿using ihi.Helpers;
using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Linq;
using Xamarin.Forms;

namespace ihi.ViewModels
{
    public class UpdateUserInfoPageViewModel : BaseViewModel
    {
        private enum EInfo
        {
            CapHoc = 1,
            Khoi,
            QuanHuyen,
            TinhThanh,
            Truong
        }
        #region Commands
        public ICommand OnClickAccept { get; set; }
        public Command<object> OnClickFocusPicker { get; set; }
        #endregion
        #region Properties
        private UpdateUserInfoPage _MainWindow;
        public UpdateUserInfoPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        private ProfileBasesViewModel _Profile = new ProfileBasesViewModel();
        public ProfileBasesViewModel Profile { get => _Profile; set { _Profile = value; OnPropertyChanged(); } }
        private ObservableCollection<Proc_GetCapHoc_Result> _ListCapHoc = new ObservableCollection<Proc_GetCapHoc_Result>();
        public ObservableCollection<Proc_GetCapHoc_Result> ListCapHoc { get => _ListCapHoc; set { _ListCapHoc = value; OnPropertyChanged(); } }
        private ObservableCollection<Proc_GetKhoi_Result> _ListKhoi = new ObservableCollection<Proc_GetKhoi_Result>();
        public ObservableCollection<Proc_GetKhoi_Result> ListKhoi { get => _ListKhoi; set { _ListKhoi = value; OnPropertyChanged(); } }
        private ObservableCollection<Proc_GetQuanHuyen_Result> _ListQuanHuyen = new ObservableCollection<Proc_GetQuanHuyen_Result>();
        public ObservableCollection<Proc_GetQuanHuyen_Result> ListQuanHuyen { get => _ListQuanHuyen; set { _ListQuanHuyen = value; OnPropertyChanged(); } }
        private ObservableCollection<Proc_GetTinhThanh_Result> _ListTinhThanh = new ObservableCollection<Proc_GetTinhThanh_Result>();
        public ObservableCollection<Proc_GetTinhThanh_Result> ListTinhThanh { get => _ListTinhThanh; set { _ListTinhThanh = value; OnPropertyChanged(); } }
        private ObservableCollection<Proc_GetTruong_Result> _ListTruong = new ObservableCollection<Proc_GetTruong_Result>();
        public ObservableCollection<Proc_GetTruong_Result> ListTruong { get => _ListTruong; set { _ListTruong = value; OnPropertyChanged(); } }

        private Proc_GetCapHoc_Result _CapHoc;
        public Proc_GetCapHoc_Result CapHoc 
        {
            get => _CapHoc; 
            set 
            {
                if (value != null && value != _CapHoc)
                {
                    _CapHoc = value;
                    Profile.IdCapHoc = value.IdCapHoc;
                    OnPropertyChanged();
                }
            }
        }
        private Proc_GetKhoi_Result _Khoi;
        public Proc_GetKhoi_Result Khoi
        {
            get => _Khoi; 
            set 
            {
                if (value != null)
                {
                    _Khoi = value;
                    Profile.Khoi = (byte?)value.IdKhoiThi;
                    OnPropertyChanged();
                }
            } 
        }
        private Proc_GetQuanHuyen_Result _QuanHuyen;
        public Proc_GetQuanHuyen_Result QuanHuyen 
        {
            get => _QuanHuyen; 
            set 
            {
                if (value != _QuanHuyen)
                {
                    _QuanHuyen = value;
                    ReloadTruong(CapHoc?.IdCapHoc, value?.IdQuanHuyen);
                    Profile.IdQuanHuyen = value?.IdQuanHuyen;
                    OnPropertyChanged();
                }
            } 
        }
        private Proc_GetTinhThanh_Result _TinhThanh;
        public Proc_GetTinhThanh_Result TinhThanh
        {
            get => _TinhThanh; 
            set
            {
                if (value != null && value != _TinhThanh)
                {
                    _TinhThanh = value;
                    ReloadQuanHuyen(value?.IdTinhThanhPho);
                    QuanHuyen = null;
                    Profile.IdTinhThanhPho = value.IdTinhThanhPho;
                    OnPropertyChanged();
                }
            } 
        }

        private async void ReloadQuanHuyen(int? idTinhThanh)
        {
            if (idTinhThanh != null)
            {
                List<Proc_GetQuanHuyen_Result> lstQuanHuyen = await SocialUtilities.GetQuanHuyen(idTinhThanh, null, (o) => { });
                ListQuanHuyen = new ObservableCollection<Proc_GetQuanHuyen_Result>();
                CollectionHelper.AddList<Proc_GetQuanHuyen_Result>(ListQuanHuyen, lstQuanHuyen);
                OnPropertyChanged(nameof(ListQuanHuyen));
            }
        }
        private async void ReloadTruong(int? idCapHoc, int? idQuanHuyen)
        {
            if (idCapHoc != null)
            {
                List<Proc_GetTruong_Result> lstTruong = await SocialUtilities.GetTruong(idCapHoc, idQuanHuyen, null, (o) => { });
                ListTruong = new ObservableCollection<Proc_GetTruong_Result>();
                CollectionHelper.AddList<Proc_GetTruong_Result>(ListTruong, lstTruong);
                OnPropertyChanged(nameof(ListTruong));
            }
        }

        private Proc_GetTruong_Result _Truong = new Proc_GetTruong_Result();
        public Proc_GetTruong_Result Truong { get => _Truong; set { _Truong = value; OnPropertyChanged(); } }
        #endregion
        public UpdateUserInfoPageViewModel(UpdateUserInfoPage mainWindow)
        {
            MainWindow = mainWindow;
            PropertyCopier<ProfileBasesViewModel, ProfileBasesViewModel>.Copy(constants.Account, Profile);
            OnPropertyChanged(nameof(Profile));
            FirstLoad();
        }

        private async void FirstLoad()
        {
            LoadCommand();
            List<Proc_GetTinhThanh_Result> lstTinhThanh = await SocialUtilities.GetTinhThanh(null, (o) => { });
            ListTinhThanh = new ObservableCollection<Proc_GetTinhThanh_Result>();
            CollectionHelper.AddList<Proc_GetTinhThanh_Result>(ListTinhThanh, lstTinhThanh);
            OnPropertyChanged(nameof(ListTinhThanh));

            List<Proc_GetCapHoc_Result> lstCapHoc = await SocialUtilities.GetCapHoc(null, (o) => { });
            ListCapHoc = new ObservableCollection<Proc_GetCapHoc_Result>();
            CollectionHelper.AddList<Proc_GetCapHoc_Result>(ListCapHoc, lstCapHoc);
            OnPropertyChanged(nameof(ListCapHoc));

            List<Proc_GetKhoi_Result> lstKhoi = await SocialUtilities.GetKhoi(null, (o) => { });
            ListKhoi = new ObservableCollection<Proc_GetKhoi_Result>();
            CollectionHelper.AddList<Proc_GetKhoi_Result>(ListKhoi, lstKhoi);
            OnPropertyChanged(nameof(ListKhoi));

            LoadSocialInfo();
        }

        private async void LoadSocialInfo()
        {
            _TinhThanh = ListTinhThanh.FirstOrDefault(x => x.IdTinhThanhPho == Profile.IdTinhThanhPho); OnPropertyChanged(nameof(TinhThanh));
            _CapHoc = ListCapHoc.FirstOrDefault(x => x.IdCapHoc == Profile.IdCapHoc); OnPropertyChanged(nameof(CapHoc));
            var quanHuyen = new Proc_GetQuanHuyen_Result { IdQuanHuyen = Profile.IdQuanHuyen, Name = await GetNameById(Profile.IdQuanHuyen, EInfo.QuanHuyen) };
            ListQuanHuyen = new ObservableCollection<Proc_GetQuanHuyen_Result>(); ListQuanHuyen.Add(quanHuyen);
            QuanHuyen = quanHuyen; OnPropertyChanged(nameof(QuanHuyen));
            var truong = new Proc_GetTruong_Result { IdTruong = Profile.IdTruong, Name = await GetNameById(Profile.IdTruong, EInfo.Truong) };
            ListTruong = new ObservableCollection<Proc_GetTruong_Result>(); ListTruong.Add(truong);
            _Truong = truong; OnPropertyChanged(nameof(Truong));
            _Khoi = ListKhoi.FirstOrDefault(x => x.IdKhoiThi == Profile.Khoi); OnPropertyChanged(nameof(Khoi));

            #region Reload Quận Huyện
            List<Proc_GetQuanHuyen_Result> lstQuanHuyen = await SocialUtilities.GetQuanHuyen(TinhThanh?.IdTinhThanhPho, null, (o) => { });
            lstQuanHuyen.Remove(quanHuyen);
            CollectionHelper.AddList<Proc_GetQuanHuyen_Result>(ListQuanHuyen, lstQuanHuyen);
            OnPropertyChanged(nameof(ListQuanHuyen));
            #endregion

            #region Reload Trường
            List<Proc_GetTruong_Result> lstTruong = await SocialUtilities.GetTruong(CapHoc?.IdCapHoc, QuanHuyen?.IdQuanHuyen, null, (o) => { });
            lstTruong.Remove(truong);
            CollectionHelper.AddList<Proc_GetTruong_Result>(ListTruong, lstTruong);
            OnPropertyChanged(nameof(ListTruong));
            #endregion


            //CapHoc = new Proc_GetCapHoc_Result { IdCapHoc = Profile.IdCapHoc, Name = await GetNameById(Profile.IdCapHoc, EInfo.CapHoc) };
            //Khoi = new Proc_GetKhoi_Result { IdKhoiThi = Profile.Khoi, Name = await GetNameById(Profile.Khoi, EInfo.Khoi) };

            //var quanHuyen = new Proc_GetQuanHuyen_Result { IdQuanHuyen = Profile.IdQuanHuyen, Name = await GetNameById(Profile.IdQuanHuyen, EInfo.QuanHuyen) };
            //ListQuanHuyen = new ObservableCollection<Proc_GetQuanHuyen_Result>(); ListQuanHuyen.Add(quanHuyen);
            //QuanHuyen = quanHuyen;

            //TinhThanh = new Proc_GetTinhThanh_Result { IdTinhThanhPho = Profile.IdTinhThanhPho, Name = await GetNameById(Profile.IdTinhThanhPho, EInfo.TinhThanh) };

            //var truong = new Proc_GetTruong_Result { IdTruong = Profile.IdTruong, Name = await GetNameById(Profile.IdTruong, EInfo.Truong) };
            //ListTruong = new ObservableCollection<Proc_GetTruong_Result>(); ListQuanHuyen.Add(quanHuyen);
            //Truong = truong;
        }

        private async Task<string> GetNameById(int? id, EInfo info)
        {
            if (id == null)
            {
                return "";
            }
            switch (info)
            {
                case EInfo.CapHoc:
                    return (await SocialUtilities.GetCapHoc(id, (o) => { })).FirstOrDefault().Name;
                case EInfo.Khoi:
                    return (await SocialUtilities.GetKhoi(id, (o) => { })).FirstOrDefault().Name;
                case EInfo.QuanHuyen:
                    return (await SocialUtilities.GetQuanHuyen(null, id, (o) => { })).FirstOrDefault().Name;
                case EInfo.TinhThanh:
                    return (await SocialUtilities.GetTinhThanh(id, (o) => { })).FirstOrDefault().Name;
                case EInfo.Truong:
                    return (await SocialUtilities.GetTruong(null, null, id, (o) => { })).FirstOrDefault().Name;
                default:
                    return "";
            }
        }

        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickAccept = new Command(async () =>
            {
                bool isOk = await AccountUtilities.UpdateUserInfo(Profile, (o) =>
                {
                    MainWindow.DisplayAlert("Cảnh báo", o, "OK");
                });
                if (isOk)
                {
                    await MainWindow.DisplayAlert("Thành công", "Cập nhật thông tin người dùng thành công!", "OK");
                }
                else
                {
                    await MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau!", "OK");
                }
                await Application.Current.MainPage.Navigation.PopAsync();
            });
            OnClickFocusPicker = new Command<object>((o) => 
            {
                if (o != null)
                {
                    var pck = o as Picker;
                    pck.Focus();
                }
            });
        }
    }
}
