﻿using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Extended;

namespace ihi.ViewModels
{
    public class ListNotifyPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnListViewRefreshing { get; set; }
        public Command<object> OnClickNotify { get; set; }
        #endregion

        #region Properties
        int page = 0; int pageSize = 5; bool continueLoading = true;

        private NotifyDetailPage notifyDetailPage;

        private ListNotifyPage _MainWindow;
        public ListNotifyPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        private string _ListViewFooter;
        public string ListViewFooter { get => _ListViewFooter; set { _ListViewFooter = value; OnPropertyChanged(); } }

        private bool _IsRefreshing = false;
        public bool IsRefreshing { get => _IsRefreshing; set { _IsRefreshing = value; OnPropertyChanged(); } }

        private InfiniteScrollCollection<Proc_GetNotify_Result> _ListNotify;
        public InfiniteScrollCollection<Proc_GetNotify_Result> ListNotify { get => _ListNotify; set { _ListNotify = value; OnPropertyChanged(); } }

        #endregion

        public ListNotifyPageViewModel(ListNotifyPage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }

        private void FirstLoad()
        {
            ListViewFooter = "Đang tải";
            LoadCommand();
            LoadNotify();
        }

        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickNotify = new Command<object>((o) =>
            {
                IsBusy = true;
                if (o != null)
                {
                    var model = o as Proc_GetNotify_Result;

                    if (model != null)
                    {
                        notifyDetailPage = new NotifyDetailPage(model);
                        LoadPage(notifyDetailPage);
                    }
                    else
                    {
                        MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau!", "OK");
                    }
                }
            });
            OnListViewRefreshing = new Command(() =>
            {
                IsRefreshing = true;
                LoadNotify();
                IsRefreshing = false;
            });
        }

        private async void LoadNotify()
        {
            page = 0; continueLoading = true;
            ListNotify = new InfiniteScrollCollection<Proc_GetNotify_Result>
            {
                OnLoadMore = async () =>
                {
                    IsBusy = true;
                    ListViewFooter = "Đang tải";
                    page = page + 1;
                    var listNotify = await NotifyUtilities.GetNotifies(page, pageSize, (o) => { });

                    if (listNotify.Count < pageSize)
                    {
                        continueLoading = false;
                        ListViewFooter = "";
                    }
                    return listNotify;
                },

                OnCanLoadMore = () =>
                {
                    return continueLoading;
                }
            };

            var firstListNotify = await NotifyUtilities.GetNotifies(page, pageSize, (o) => { });
            if (firstListNotify != null)
            {
                if (firstListNotify.Count == 0)
                {
                    ListViewFooter = "Không có thông báo";
                }
                ListNotify.AddRange(firstListNotify);
                OnPropertyChanged(nameof(ListNotify));
            }
        }
    }
}
