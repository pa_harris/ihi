﻿using ihi.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace ihi.ViewModels
{
    public class ExamDetailedFilePageViewModel : BaseViewModel
    {
        #region Commands

        #endregion
        #region Properties
        private ExamDetailedFilePage _MainWindow;
        public ExamDetailedFilePage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        #endregion
        public ExamDetailedFilePageViewModel(ExamDetailedFilePage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }

        private void FirstLoad()
        {
            LoadCommand();
        }

        private void LoadCommand()
        {
            base.InitializeCommands();
        }
    }
}
