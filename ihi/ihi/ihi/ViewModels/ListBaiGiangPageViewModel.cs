﻿using ihi.Helpers;
using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace ihi.ViewModels
{
    public class ListBaiGiangPageViewModel : BaseViewModel
    {
        #region Commands
        public Command<object> OnClickLearn { get; set; }
        #endregion

        #region Properties
        private ListBaiGiangPage _MainWindow;
        public ListBaiGiangPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        private bool _IsBought;
        public bool IsBought { get => _IsBought; set { _IsBought = value; OnPropertyChanged(); } }
        private ChuongBindingModel _Chuong;
        public ChuongBindingModel Chuong { get => _Chuong; set { _Chuong = value; OnPropertyChanged(); } }
        private ObservableCollection<BaiGiangBindingModel> _ListBaiGiang = new ObservableCollection<BaiGiangBindingModel>();
        public ObservableCollection<BaiGiangBindingModel> ListBaiGiang { get => _ListBaiGiang; set { _ListBaiGiang = value; OnPropertyChanged(); } }
        #endregion

        public ListBaiGiangPageViewModel(ListBaiGiangPage mainWindow, ChuongBindingModel model = null, bool isBought = false)
        {
            MainWindow = mainWindow;
            Chuong = model;
            IsBought = isBought;
            FirstLoad();
        }

        private async void FirstLoad()
        {
            LoadCommand();
            var lst = await BaiGiangUtilities.GetBaiGiangByMucLucId(Chuong.MuclucId, (o) =>
            {
                MainWindow.DisplayAlert("Cảnh báo", o, "OK");
            });
            CollectionHelper.AddList<BaiGiangBindingModel>(ListBaiGiang, lst);
        }

        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickLearn = new Command<object>((o) =>
            {
                IsBusy = true;
                if (IsBought)
                {
                    if (o != null)
                    {
                        BaiGiangBindingModel baiGiang = o as BaiGiangBindingModel;
                        if (!string.IsNullOrEmpty(baiGiang?.BaigiangId))
                        {
                            LoadPage(new BaiGiangVideoPage(baiGiang));
                        }
                        else
                        {
                            MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau!", "OK");
                        }
                    }
                }
                else
                {
                    MainWindow.DisplayAlert("Cảnh báo", "Vui lòng mua khóa học trước khi học!", "OK");
                }
                IsBusy = false;
            });
        }
    }
}
