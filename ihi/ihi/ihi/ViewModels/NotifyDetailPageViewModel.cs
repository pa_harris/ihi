﻿using ihi.Models;
using ihi.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ihi.ViewModels
{
    public class NotifyDetailPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnListViewRefreshing { get; set; }
        public Command<object> OnClickNotify { get; set; }
        #endregion

        #region Properties
        private NotifyDetailPage _MainWindow;
        public NotifyDetailPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        private Proc_GetNotify_Result _NotifyDetail;
        public Proc_GetNotify_Result NotifyDetail { get => _NotifyDetail; set { _NotifyDetail = value; OnPropertyChanged(); } }

        private bool _IsRefreshing = false;
        public bool IsRefreshing { get => _IsRefreshing; set { _IsRefreshing = value; OnPropertyChanged(); } }

        #endregion

        public NotifyDetailPageViewModel(NotifyDetailPage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }
        public NotifyDetailPageViewModel(NotifyDetailPage mainWindow, Proc_GetNotify_Result model)
        {
            MainWindow = mainWindow;
            NotifyDetail = model;
            FirstLoad();
        }

        private void FirstLoad()
        {
            LoadCommand();
        }

        private void LoadCommand()
        {
            base.InitializeCommands();
        }
    }
}
