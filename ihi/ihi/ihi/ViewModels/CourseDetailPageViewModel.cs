﻿using ihi.Helpers;
using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using ihi.Views.Popups;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ihi.ViewModels
{
    public class CourseDetailPageViewModel : BaseViewModel
    {
        #region Commands
        public Command<object> OnClickOpenListBaiGiang { get; set; }
        public ICommand OnClickPayment { get; set; }
        public ICommand OnClickBuy { get; set; }
        #endregion

        #region Properties
        private CourseDetailPage _MainWindow;
        public CourseDetailPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        private bool _IsBought;
        public bool IsBought { get => _IsBought; set { _IsBought = value; OnPropertyChanged(); } }

        private CourseBindingModel _Course;
        public CourseBindingModel Course { get => _Course; set { _Course = value; OnPropertyChanged(); } }
        private ProfileTeacherViewModel _Teacher;
        public ProfileTeacherViewModel Teacher { get => _Teacher; set { _Teacher = value; OnPropertyChanged(); } }
        private ObservableCollection<string> _Mota = new ObservableCollection<string>();
        public ObservableCollection<string> Mota { get => _Mota; set { _Mota = value; OnPropertyChanged(); } }
        private ObservableCollection<ChuongBindingModel> _ListChapter = new ObservableCollection<ChuongBindingModel>();
        public ObservableCollection<ChuongBindingModel> ListChapter { get => _ListChapter; set { _ListChapter = value; OnPropertyChanged(); } }
        #endregion

        public CourseDetailPageViewModel(CourseDetailPage mainWindow, CourseBindingModel model = null, bool isBought = false)
        {
            MainWindow = mainWindow;
            Course = model;
            IsBought = isBought;
            FirstLoad();
        }

        private async void FirstLoad()
        {
            LoadCommands();
            Teacher = (await UserUtilities.GetListTeacher(1, 1, (p) => { }, Course.UserId)).FirstOrDefault();
            if (!string.IsNullOrEmpty(Teacher.About))
            {
                Teacher.About.Replace("\\n", "").Replace("\\r", ""); OnPropertyChanged(nameof(Teacher.About));
            }
            string[] newlineChar = { "\\n" };
            Mota = new ObservableCollection<string>(Course.Mota.Split(newlineChar, StringSplitOptions.RemoveEmptyEntries));
            var lstChapter = await MucLucChuongUtilities.GetMucLucByKhoaHocId(Course.KhoahocId, (p) => { });
            CollectionHelper.AddList<ChuongBindingModel>(ListChapter, lstChapter);            
        }


        private void LoadCommands()
        {
            base.InitializeCommands();
            OnClickOpenListBaiGiang = new Command<object>((o) =>
            {
                IsBusy = true;
                if (o != null)
                {
                    var model = o as ChuongBindingModel;
                    if (model != null)
                    {
                        LoadPage(new ListBaiGiangPage(model, IsBought));
                        return;
                    }
                }
                MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau!", "OK");
            });
            OnClickPayment = new Command(async () =>
            {
                IsBusy = true;
                bool isEnough = false;
                if (Course?.Gia == null)
                {
                    // Giá null = miễn phí
                    isEnough = true;
                }
                else
                {
                    // Giá khác null
                    if (!string.IsNullOrEmpty(constants.AccessToken))
                    {
                        await AccountUtilities.UserInfo((p) => { });
                        if (Course?.Chietkhau == null)
                        {
                            // Chiết khấu null kiểm tra theo giá
                            isEnough = constants.Account.Money >= Course?.Gia;
                        }
                        else
                        {
                            // Chiết khấu khác null
                            isEnough = constants.Account.Money >= (Course?.Gia * (decimal)(Course?.Chietkhau / 100));
                        }
                    }
                    else
                    {
                        await MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau.", "OK");
                        IsBusy = false;
                        return;
                    }
                }

                if (!isEnough)
                {
                    await MainWindow.DisplayAlert("Thông báo", "Tiền trong tài khoản của bạn không đủ để thực hiện mua khóa học. Vui lòng nạp thêm tiền để sử dụng dịch vụ.", "OK");
                    IsBusy = false;
                    return;
                }
                else
                {
                    Rg.Plugins.Popup.Services.PopupNavigation.PushAsync(new ConfirmBuyCoursePopup(this));
                }
            });
            OnClickBuy = new Command(async() =>
            {
                IsBusy = true;
                Rg.Plugins.Popup.Services.PopupNavigation.PopAllAsync();
                if (!string.IsNullOrEmpty(Course?.KhoahocId))
                {
                    bool isBought = await CourseUtilities.BuyCourse(Course.KhoahocId, (o) =>
                    {
                        MainWindow.DisplayAlert("Cảnh báo", o, "OK");
                        return;
                    });
                    if (isBought)
                    {
                        MainWindow.DisplayAlert("Thông báo", "Mua khóa học thành công!", "OK");
                        await AccountUtilities.UserInfo((p) => { });
                        LoadPage(new HomePage());
                        NavigationHelper.RemoveAllPageBefore();
                    }
                    //else
                    //{
                    //    MainWindow.DisplayAlert("Cảnh báo", "Mua khóa học không thành công!", "OK");
                    //}
                }
                else
                {
                    MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau!", "OK");
                }
                IsBusy = false;
            });
        }
    }
}
