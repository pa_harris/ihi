﻿using ihi.Helpers;
using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ihi.ViewModels
{
    public class HomePageViewModel : BaseViewModel
    {
        #region Commands
        public Command<object> OnClickSelectLop { get; set; }
        public ICommand OnClickCourseItem { get; set; }
        public ICommand OnClickGroupCourseItem { get; set; }
        public ICommand OnClickAvatar { get; set; }
        public ICommand OnClickLearning { get; set; }
        public ICommand OnClickGroupCourse { get; set; }
        public ICommand OnClickExam { get; set; }
        public ICommand OnClickPayment { get; set; }
        #endregion

        #region Properties
        private ListBoughtCoursePage listBoughtCoursePage;
        private UserSettingPage userSettingPage;
        private ListGroupCoursePage listGroupCoursePage;
        private ListCoursePage listCoursePage;
        private PaymentPage paymentPage;
        private TracNghiemPage tracNghiemPage;

        private HomePage _MainWindow;
        public HomePage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        private ProfileBasesViewModel _Account;
        public ProfileBasesViewModel Account { get => _Account; set { _Account = value; OnPropertyChanged(); } }
        private ObservableCollection<SliderBindingModel> _ListAdImage;
        public ObservableCollection<SliderBindingModel> ListAdImage { get => _ListAdImage; set { _ListAdImage = value; OnPropertyChanged(); } }
        private ObservableCollection<CourseBindingModel> _ListBestSellingCourse;
        public ObservableCollection<CourseBindingModel> ListBestSellingCourse { get => _ListBestSellingCourse; set { _ListBestSellingCourse = value; OnPropertyChanged(); } }
        private ObservableCollection<CourseBindingModel> _ListDiscountCourse;
        public ObservableCollection<CourseBindingModel> ListDiscountCourse { get => _ListDiscountCourse; set { _ListDiscountCourse = value; OnPropertyChanged(); } }
        private ObservableCollection<PopularCourseBindingModel> _ListPopularCourse;
        public ObservableCollection<PopularCourseBindingModel> ListPopularCourse { get => _ListPopularCourse; set { _ListPopularCourse = value; OnPropertyChanged(); } }
        private ObservableCollection<Proc_GetListModPagination_Result> _ListMod;
        public ObservableCollection<Proc_GetListModPagination_Result> ListMod { get => _ListMod; set { _ListMod = value; OnPropertyChanged(); } }
        private ObservableCollection<VoucherBindingModel> _ListVoucher;
        public ObservableCollection<VoucherBindingModel> ListVoucher { get => _ListVoucher; set { _ListVoucher = value; OnPropertyChanged(); } }
        private ObservableCollection<GroupCourseViewModel> _ListGroupCourse;
        public ObservableCollection<GroupCourseViewModel> ListGroupCourse { get => _ListGroupCourse; set { _ListGroupCourse = value; OnPropertyChanged(); } }
        private ObservableCollection<LopViewModel> _ListLop;
        public ObservableCollection<LopViewModel> ListLop { get => _ListLop; set { _ListLop = value; OnPropertyChanged(); } }
        private ObservableCollection<ClientCommentViewModel> _ListClientComment;
        public ObservableCollection<ClientCommentViewModel> ListClientComment { get => _ListClientComment; set { _ListClientComment = value; OnPropertyChanged(); } }
        private LopViewModel _LopSelected;
        public LopViewModel LopSelected
        {
            get => _LopSelected; 
            set
            {
                if (value != _LopSelected && value != null)
                {
                    IsBusy = true;
                    LoadGroupCourse(value.LopId);
                    IsBusy = false;
                }
                _LopSelected = value;
                OnPropertyChanged();
            }
        }
        #endregion
        public HomePageViewModel(HomePage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoadAsync();
        }

        private async void FirstLoadAsync()
        {
            LoadCommands();
            Account = constants.Account;
            List<SliderBindingModel> lstAd = await CommonUtilities.GetHomeSlider((p) => { });
            ListAdImage = new ObservableCollection<SliderBindingModel>();
            CollectionHelper.AddList<SliderBindingModel>(ListAdImage, lstAd);
            OnPropertyChanged(nameof(ListAdImage));

            List<CourseBindingModel> lstCourse = await CourseUtilities.GetBestSellingCourses(null, null, null, (p) => { });
            ListBestSellingCourse = new ObservableCollection<CourseBindingModel>();
            CollectionHelper.AddList<CourseBindingModel>(ListBestSellingCourse, lstCourse);
            OnPropertyChanged(nameof(ListBestSellingCourse));

            lstCourse = await CourseUtilities.GetDiscountCourses(null, null, null, (p) => { });
            ListDiscountCourse = new ObservableCollection<CourseBindingModel>();
            CollectionHelper.AddList<CourseBindingModel>(ListDiscountCourse, lstCourse);
            OnPropertyChanged(nameof(ListDiscountCourse));

            List<Proc_GetListModPagination_Result> lstMod = await UserUtilities.GetListMod(1, 5, (p) => { });
            ListMod = new ObservableCollection<Proc_GetListModPagination_Result>();
            CollectionHelper.AddList<Proc_GetListModPagination_Result>(ListMod, lstMod);
            OnPropertyChanged(nameof(ListMod));

            List<PopularCourseBindingModel> lstPopularCourse = await CourseUtilities.GetListPopularGroupCourse(1, 5, (p) => { });
            ListPopularCourse = new ObservableCollection<PopularCourseBindingModel>();
            CollectionHelper.AddList<PopularCourseBindingModel>(ListPopularCourse, lstPopularCourse);
            OnPropertyChanged(nameof(ListPopularCourse));

            List<VoucherBindingModel> lstVoucher = await VoucherUtilities.GetListVoucher(1, 5, (p) => { });
            ListVoucher = new ObservableCollection<VoucherBindingModel>();
            CollectionHelper.AddList<VoucherBindingModel>(ListVoucher, lstVoucher);
            OnPropertyChanged(nameof(ListVoucher));

            List<ClientCommentViewModel> lstComment = await CommonUtilities.GetListComment((p) => { });
            ListClientComment = new ObservableCollection<ClientCommentViewModel>();
            CollectionHelper.AddList<ClientCommentViewModel>(ListClientComment, lstComment);
            OnPropertyChanged(nameof(ListClientComment));

            FirstLoadListGroupCourse();
        }

        private async void FirstLoadListGroupCourse()
        {
            #region Load lớp
            var lstLop = await LopUtilities.GetListLop("", (p) => { });
            ListLop = new ObservableCollection<LopViewModel>();
            CollectionHelper.AddList<LopViewModel>(ListLop, lstLop);
            OnPropertyChanged(nameof(ListLop));
            _LopSelected = ListLop.FirstOrDefault(); OnPropertyChanged(nameof(LopSelected));
            LoadGroupCourse(LopSelected.LopId);
            #endregion
        }

        private async void LoadGroupCourse(string lopId, int? parentId = null)
        {
            var lstGroupCourse = await GroupCourseUtilities.GetListGroupCourse(lopId, false, null, 1, 5, (p) => { });
            ListGroupCourse = new ObservableCollection<GroupCourseViewModel>();
            CollectionHelper.AddList<GroupCourseViewModel>(ListGroupCourse, lstGroupCourse);
            ListGroupCourse.Add(new GroupCourseViewModel { LopId = LopSelected.LopId, Tieude = "", Title = "Thêm", UrlImg = "category.png" });
            OnPropertyChanged(nameof(ListGroupCourse));
        }


        private void LoadCommands()
        {
            base.InitializeCommands();
            OnClickSelectLop = new Command<object>((o) =>
            {
                if (o != null)
                {
                    var picker = o as Picker;
                    picker.IsEnabled = true;
                    picker.Focus();
                }
            });
            OnClickAvatar = new Command(() =>
            {
                IsBusy = true;
                if (userSettingPage?.ViewModel == null)
                {
                    userSettingPage = new UserSettingPage();
                }
                LoadPage(userSettingPage);
            });
            OnClickLearning = new Command(() =>
            {
                IsBusy = true;
                if (listBoughtCoursePage?.ViewModel == null)
                {
                    listBoughtCoursePage = new ListBoughtCoursePage();
                }
                LoadPage(listBoughtCoursePage);
            });
            OnClickGroupCourse = new Command(() =>
            {
                IsBusy = true;
                LoadPage(new ListGroupCoursePage());
            });
            OnClickExam = new Command(() =>
            {
                IsBusy = true;
                if (tracNghiemPage?.ViewModel == null)
                {
                    tracNghiemPage = new TracNghiemPage();
                }
                LoadPage(tracNghiemPage);
            });
            OnClickPayment = new Command(() =>
            {
                IsBusy = true;
                if (paymentPage?.ViewModel == null)
                {
                    paymentPage = new PaymentPage();
                }
                LoadPage(paymentPage);
            });

            OnClickGroupCourseItem = new Command<object>((o) =>
            {
                if (o != null)
                {
                    var model = o as GroupCourseViewModel;
                    if (model.Title.Equals("Thêm"))
                    {
                        //LoadPage(new ListGroupCoursePage(model.LopId, ListGroupCourse.FirstOrDefault().Id));
                        LoadPage(new ListGroupCoursePage(model.LopId, null));
                    }
                    else
                    {
                        LoadPage(new ListGroupCoursePage(model.LopId, model.Id));
                    }                    
                }
                else
                {
                    IsBusy = false;
                }
            });
            OnClickCourseItem = new Command<object>((o) =>
            {
                if (o != null)
                {
                    IsBusy = true;
                    var model = o as CourseBindingModel;

                    if (model != null)
                    {
                        LoadPage(new CourseDetailPage(model));
                    }
                    else
                    {
                        MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau!", "OK");
                    }
                }
            });
        }
    }
}
