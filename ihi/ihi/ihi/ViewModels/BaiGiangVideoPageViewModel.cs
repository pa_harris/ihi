﻿using ihi.Helpers;
using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using MediaManager;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ihi.ViewModels
{
    public class BaiGiangVideoPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand PageDisappearing { get; set; }
        #endregion
        #region Properties
        private BaiGiangVideoPage _MainWindow;
        public BaiGiangVideoPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        //private string _BaiGiangId;
        //public string BaiGiangId { get => _BaiGiangId; set { _BaiGiangId = value; OnPropertyChanged(); } }
        private string _UrlVideo;
        public string UrlVideo { get => _UrlVideo; set { _UrlVideo = value; OnPropertyChanged(); } }
        private BaiGiangBindingModel _BaiGiang;
        public BaiGiangBindingModel BaiGiang { get => _BaiGiang; set { _BaiGiang = value; OnPropertyChanged(); } }
        #endregion
        public BaiGiangVideoPageViewModel(BaiGiangVideoPage mainWindow, BaiGiangBindingModel baiGiang = null)
        {
            MainWindow = mainWindow;
            BaiGiang = baiGiang;
            UrlVideo = $"{constants.DomainAPIUrl}/api/Stream/GetVideoContent?token={constants.AccessToken}&lid={BaiGiang.BaigiangId}";
            //UrlVideo = $"{constants.DomainAPIUrl}/apivideo/play?lid={BaiGiangId}&token={constants.AccessToken}";
            FirstLoad();
        }

        private void FirstLoad()
        {
            LoadCommand();
        }
        private void LoadCommand()
        {
            base.InitializeCommands();
            PageDisappearing = new Command(PageDisappeared);
        }
        private void PageDisappeared()
        {
            CrossMediaManager.Current.Stop();
            CrossMediaManager.Current.Dispose();
        }
    }
}
