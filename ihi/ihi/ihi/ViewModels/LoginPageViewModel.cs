﻿using ihi.Helpers;
using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using Plugin.FacebookClient;
using Plugin.GoogleClient;
using Plugin.GoogleClient.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ihi.ViewModels
{
    public class LoginPageViewModel : BaseViewModel
    {
        #region Facebook login region
        string[] FacebookPermisions = new string[] { "email", "public_profile", "user_posts" };
        public FacebookProfile facebookProfile { get; set; }
        #endregion

        #region Google login region
        //string[] FacebookPermisions = new string[] { "email", "public_profile", "user_posts" };
        public GoogleProfile googleProfile { get; set; }
        #endregion

        #region Commands
        public ICommand OnClickLogin { get; set; }
        public ICommand OnClickForgotPassword { get; set; }
        public ICommand OnClickRegister { get; set; }
        public ICommand OnClickLoginFacebook { get; set; }
        public ICommand OnClickLoginGoogle { get; set; }
        #endregion
        #region Properties
        private TabMainPage tabMainPage;
        private ForgotPasswordPage forgotPasswordPage;
        private RegisterPage registerPage;
        private LoginPage _MainWindow;
        public LoginPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        private string _Username;
        public string Username { get => _Username; set { _Username = value; OnPropertyChanged(); } }
        private string _Password;
        public string Password { get => _Password; set { _Password = value; OnPropertyChanged(); } }
        #endregion
        public LoginPageViewModel(LoginPage mainWindow, string userName)
        {
            MainWindow = mainWindow;
            Username = userName;
            FirstLoad();
        }
        private void FirstLoad()
        {
            LoadCommand();
        }
        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickLogin = new Command(async () =>
            {
                IsBusy = true;
                if (ValidateLoginData())
                {
                    bool isLoginSucceed = await AccountUtilities.Login(new AccountLoginModel { UserName = this.Username, Password = this.Password }, (error) => 
                    {
                        MainWindow.DisplayAlert("Cảnh báo", error, "OK");
                        IsBusy = false;
                        return;
                    });
                    if (isLoginSucceed)
                    {
                        var userInfo = await AccountUtilities.UserInfo((error) =>
                        {
                            MainWindow.DisplayAlert("Cảnh báo", error, "OK");
                            IsBusy = false;
                            return;
                        });
                        if (userInfo != null)
                        {
                            constants.Account = userInfo;
                            if (tabMainPage == null)
                            {
                                tabMainPage = new TabMainPage();
                            }
                            LoadPage(tabMainPage);
                            NavigationHelper.RemoveAllPageBefore();
                        }
                        else
                        {
                            MainWindow.DisplayAlert("Cảnh báo", "Đăng nhập thất bại. Vui lòng đăng nhập lại.", "OK");
                            IsBusy = false;
                            return;
                        }
                    }
                    else
                    {
                        IsBusy = false;
                    }
                }
                else
                {
                    IsBusy = false;
                }
            });
            OnClickForgotPassword = new Command(() =>
            {
                IsBusy = true;
                if (forgotPasswordPage == null || forgotPasswordPage.ViewModel == null)
                {
                    forgotPasswordPage = new ForgotPasswordPage(Username);
                }
                else
                {
                    if (forgotPasswordPage.ViewModel.Username != Username)
                    {
                        forgotPasswordPage.ViewModel.Username = Username;
                    }
                }
                LoadPage(forgotPasswordPage);
            });
            OnClickRegister = new Command(() =>
            {
                IsBusy = true;
                if (registerPage == null)
                {
                    registerPage = new RegisterPage();
                }
                LoadPage(registerPage);
            });
            OnClickLoginFacebook = new Command(async () =>
            {
                IsBusy = true;
                FacebookResponse<bool> response = await CrossFacebookClient.Current.LoginAsync(FacebookPermisions);
                switch (response.Status)
                {
                    case FacebookActionStatus.Completed:
                        OnExternalAuthCompleted(LoginProvider.Facebook, CrossFacebookClient.Current.ActiveToken);
                        break;
                    case FacebookActionStatus.Canceled:
                        await MainWindow.DisplayAlert("Canceled", response.Message, "Ok");
                        break;
                    case FacebookActionStatus.Unauthorized:
                        await MainWindow.DisplayAlert("Unauthorized", response.Message, "Ok");
                        break;
                    case FacebookActionStatus.Error:
                        await MainWindow.DisplayAlert("Error", response.Message, "Ok");
                        break;
                }
            });
            OnClickLoginGoogle = new Command(async () =>
            {
                IsBusy = true;
                CrossGoogleClient.Current.OnLogin += OnGoogleLoginCompleted;
                
                try
                {
                    await CrossGoogleClient.Current.LoginAsync();
                }
                catch (GoogleClientSignInNetworkErrorException ee)
                {
                    await App.Current.MainPage.DisplayAlert("Error", ee.Message, "OK");
                }
                catch (GoogleClientSignInCanceledErrorException ee)
                {
                    await App.Current.MainPage.DisplayAlert("Error", ee.Message, "OK");
                }
                catch (GoogleClientSignInInvalidAccountErrorException ee)
                {
                    await App.Current.MainPage.DisplayAlert("Error", ee.Message, "OK");
                }
                catch (GoogleClientSignInInternalErrorException ee)
                {
                    await App.Current.MainPage.DisplayAlert("Error", ee.Message, "OK");
                }
                catch (GoogleClientNotInitializedErrorException ee)
                {
                    await App.Current.MainPage.DisplayAlert("Error", ee.Message, "OK");
                }
                catch (GoogleClientBaseException ee)
                {
                    await App.Current.MainPage.DisplayAlert("Error", ee.Message, "OK");
                }
            });
        }

        private bool ValidateLoginData()
        {
            if (string.IsNullOrEmpty(Username))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Tên đăng nhập không được trống!", "OK");
                return false;
            }
            if (string.IsNullOrEmpty(Password))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Mật khẩu không được trống!", "OK");
                return false;
            }
            return true;
        }

        private async void OnExternalAuthCompleted(LoginProvider loginProvider, string externalAccessToken)
        {
            ExternalLoginBindingModel model = new ExternalLoginBindingModel { LoginProvider = loginProvider.ToString(), ExternalAccessToken = externalAccessToken };
            bool isLoggedIn = await AccountUtilities.ExternalLogins(model, (error) =>
            {
                MainWindow.DisplayAlert("Cảnh báo", error, "OK");
                IsBusy = false;
            });

            if (isLoggedIn)
            {
                var userInfo = await AccountUtilities.UserInfo((error) =>
                {
                    MainWindow.DisplayAlert("Cảnh báo", error, "OK");
                    IsBusy = false;
                    return;
                });
                if (userInfo != null)
                {
                    constants.Account = userInfo;
                    if (tabMainPage == null)
                    {
                        tabMainPage = new TabMainPage();
                    }
                    LoadPage(tabMainPage);
                    NavigationHelper.RemoveAllPageBefore();
                }
                else
                {
                    MainWindow.DisplayAlert("Cảnh báo", "Đăng nhập thất bại. Vui lòng đăng nhập lại.", "OK");
                    IsBusy = false;
                    return;
                }
            }
                        
            //switch (loginProvider)
            //{
            //    case LoginProvider.Facebook:

            //        break;
            //    case LoginProvider.Google:
            //        break;
            //    case LoginProvider.Apple:
            //        break;
            //    default:
            //        break;
            //}
        }

        private void OnGoogleLoginCompleted(object sender, GoogleClientResultEventArgs<GoogleUser> loginEventArgs)
        {
            if (loginEventArgs.Data != null)
            {
                OnExternalAuthCompleted(LoginProvider.Google, CrossGoogleClient.Current.AccessToken);
            }
            else
            {
                App.Current.MainPage.DisplayAlert("Error", loginEventArgs.Message, "OK");
            }

            CrossGoogleClient.Current.OnLogin -= OnGoogleLoginCompleted;
        }
    }
}
