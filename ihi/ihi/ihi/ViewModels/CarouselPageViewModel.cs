﻿using ihi.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using static ihi.Views.CarouselPage;

namespace ihi.ViewModels
{
    public class CarouselPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickCreateAccount { get; set; }
        public ICommand OnClickLogin { get; set; }
        public ICommand OnClickSkip { get; set; }
        #endregion
        #region Properties
        private RegisterPage registerPage;
        private LoginPage loginPage;
        private Views.CarouselPage _MainWindow;
        public Views.CarouselPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        private List<CarouselPageModel> _ListIndicatorView;
        public List<CarouselPageModel> ListIndicatorView { get => _ListIndicatorView; set { _ListIndicatorView = value; OnPropertyChanged(); } }
        #endregion
        public CarouselPageViewModel(Views.CarouselPage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }
        private void FirstLoad()
        {
            ListIndicatorView = new List<CarouselPageModel>()
            {
                new CarouselPageModel { Index = 1, Image = "indicator1.png", Content = $"Cùng ihi.vn chinh phục những kiến thức khó.\nĐăng nhập để sử dung những tính năng tuyệt vời nhé!"},
                new CarouselPageModel { Index = 2, Image = "indicator2.png", Content = $"Dễ dàng sử dụng và đăng nhập trên nhiều thiết bị.\nKhông giới hạn không gian, thời gian học."},
                new CarouselPageModel { Index = 3, Image = "indicator3.png", Content = $"Tối ưu nội dung và bổ trợ kiến thức tất cả các môn học một cách cực kỳ dễ hiểu."}
            };
            LoadCommand();
        }
        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickCreateAccount = new Command(() => 
            {
                IsBusy = true;
                if (registerPage == null)
                {
                    registerPage = new RegisterPage();
                }
                LoadPage(registerPage);
            });
            OnClickLogin = new Command(() =>
            {
                IsBusy = true;
                if (loginPage == null)
                {
                    loginPage = new LoginPage();
                }
                LoadPage(loginPage);
            });
            OnClickSkip = new Command(() =>
            {
                IsBusy = true;
                if (loginPage == null)
                {
                    loginPage = new LoginPage();
                }
                LoadPage(loginPage);
            });
        }
    }
}
