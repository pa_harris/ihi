﻿using ihi.Utilities;
using ihi.Views;
using ihi.Views.Popups;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ihi.ViewModels
{
    public class ForgotPasswordPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickResetPassword { get; set; }
        public ICommand OnClickOpenOTP { get; set; }
        #endregion
        #region Properties
        private OTPConfirmationPage otpPage;
        private ForgotPasswordPage _MainWindow;
        public ForgotPasswordPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        private string _Username;
        public string Username { get => _Username; set { _Username = value; OnPropertyChanged(); } }
        #endregion
        public ForgotPasswordPageViewModel(ForgotPasswordPage mainWindow, string userName)
        {
            MainWindow = mainWindow;
            Username = userName;
            FirstLoad();
        }
        private void FirstLoad()
        {
            LoadCommand();
        }
        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickResetPassword = new Command(() =>
            {
                Rg.Plugins.Popup.Services.PopupNavigation.PushAsync(new CheckEmailPopup(this));
            });
            OnClickOpenOTP = new Command(async () =>
            {
                IsBusy = true;
                Rg.Plugins.Popup.Services.PopupNavigation.PopAllAsync();
                bool isSentOTP = await AccountUtilities.ForgotPassword(new Models.ForgotPasswordBindingModel { Email = Username }, (error) =>
                {
                    MainWindow.DisplayAlert("Cảnh báo", error, "OK");
                    IsBusy = false;
                    return;
                });
                if (isSentOTP)
                {
                    await MainWindow.DisplayAlert("Thành công", "Gửi OTP thành công! Vui lòng kiểm tra trong hộp thư!", "OK");
                    if (otpPage == null || otpPage.ViewModel == null)
                    {
                        otpPage = new OTPConfirmationPage(Username);
                    }
                    else
                    {
                        if (otpPage.ViewModel.UserName != Username)
                        {
                            otpPage.ViewModel.UserName = Username;
                        }
                    }
                    LoadPage(otpPage);
                }
                else
                {
                    MainWindow.DisplayAlert("Cảnh báo", "Có lỗi xảy ra! Vui lòng quay lại sau.", "OK");
                    IsBusy = false;
                    return;
                }
            });
        }

    }
}
