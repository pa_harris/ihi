﻿using ihi.Models;
using ihi.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ihi.ViewModels
{
    public class TracNghiemPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickOpenDeThi { get; set; }
        public ICommand OnClickHistory { get; set; }
        #endregion
        #region Properties
        private TracNghiemPage _MainWindow;
        public TracNghiemPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        #endregion
        public TracNghiemPageViewModel(TracNghiemPage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }

        private void FirstLoad()
        {
            LoadCommand();
        }

        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickOpenDeThi = new Command<object>((o) =>
            {
                IsBusy = true;
                if (o != null && !string.IsNullOrEmpty(o.ToString()))
                {
                    if (o.ToString().Contains("ontap"))
                    {
                        LoadPage(new ListDeThiPage(EDeThiType.OnTap));
                    }
                    else if (o.ToString().Contains("dethi"))
                    {
                        LoadPage(new ListDeThiPage(EDeThiType.DeThi));
                    }
                }
            });
            OnClickHistory = new Command(() =>
            {
                IsBusy = true;
                LoadPage(new ListHistoryTracNghiemPage());
            });
        }
    }
}
