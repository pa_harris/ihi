﻿using ihi.Helpers;
using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Extended;

namespace ihi.ViewModels
{
    public class ListGroupCoursePageViewModel : BaseViewModel
    {
        #region Commands
        public Command<object> OnClickSelectLop { get; set; }
        public ICommand OnListViewRefreshing { get; set; }
        public Command<object> OnClickGroupCourse { get; set; }
        #endregion

        #region Properties
        int page = 1; int pageSize = 1000; public bool continueLoading { get; set; } = true;

        private ListCoursePage listCoursePage;
        private ListGroupCoursePage _MainWindow;
        public ListGroupCoursePage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        private string _ListViewFooter;
        public string ListViewFooter { get => _ListViewFooter; set { _ListViewFooter = value; OnPropertyChanged(); } }

        private ObservableCollection<LopViewModel> _ListLop;
        public ObservableCollection<LopViewModel> ListLop { get => _ListLop; set { _ListLop = value; OnPropertyChanged(); } }
        private ObservableCollection<GroupCourseViewModel> _ListMon;
        public ObservableCollection<GroupCourseViewModel> ListMon { get => _ListMon; set { _ListMon = value; OnPropertyChanged(); } }
        private ObservableCollection<object> _PickerCollection = new ObservableCollection<object>();
        public ObservableCollection<object> PickerCollection { get => _PickerCollection; set { _PickerCollection = value; OnPropertyChanged(); } }
        private ObservableCollection<GroupCourseViewModel> _ListGroupCourse;
        public ObservableCollection<GroupCourseViewModel> ListGroupCourse { get => _ListGroupCourse; set { _ListGroupCourse = value; OnPropertyChanged(); } }

        private bool _IsRefreshing = false;
        public bool IsRefreshing { get => _IsRefreshing; set { _IsRefreshing = value; OnPropertyChanged(); } }

        //private LopViewModel _LopSelected;
        //public LopViewModel LopSelected { get => _LopSelected; set 
        //    {
        //        if (value != _LopSelected && value != null)
        //        {
        //            IsBusy = true;
        //            LoadGroupCourse(value.LopId);
        //            IsBusy = false;
        //        }
        //        _LopSelected = value;
        //        OnPropertyChanged();
        //    }
        //}

        private object _PickerSelected;
        public object PickerSelected
        {
            get => _PickerSelected; 
            set
            {
                if (value.GetType().Equals(typeof(LopViewModel)))
                {
                    LopViewModel valueCasted = value as LopViewModel;
                    if ((LopViewModel)valueCasted != _PickerSelected && valueCasted != null)
                    {
                        IsBusy = true;
                        LoadGroupCourse(valueCasted.LopId);
                        IsBusy = false;
                    }
                    _PickerSelected = value;
                    OnPropertyChanged();
                }
                else
                {
                    GroupCourseViewModel valueCasted = value as GroupCourseViewModel;
                    if ((GroupCourseViewModel)valueCasted != _PickerSelected && valueCasted != null)
                    {
                        IsBusy = true;
                        LoadGroupCourse(valueCasted.LopId, valueCasted.Id);
                        IsBusy = false;
                    }
                    _PickerSelected = value;
                    OnPropertyChanged();
                }
            }
        }


        private string _PickerTitle = "Lớp học:";
        public string PickerTitle { get => _PickerTitle; set { _PickerTitle = value; OnPropertyChanged(); } }
        #endregion

        public ListGroupCoursePageViewModel(ListGroupCoursePage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }
        public ListGroupCoursePageViewModel(ListGroupCoursePage mainWindow, string lopId, int? parentGroupCourseId)
        {
            MainWindow = mainWindow;
            FirstLoad(lopId, parentGroupCourseId);
        }

        private async void FirstLoad()
        {
            LoadCommands();
            PickerTitle = "Lớp học:";
            ListViewFooter = "Đang tải";

            #region Load lớp
            ListLop = new ObservableCollection<LopViewModel>();
            var lstLop = await LopUtilities.GetListLop("", (p) => { });
            if (lstLop != null)
            {
                foreach (var item in lstLop)
                {
                    if (item != null)
                    {
                        ListLop.Add(item);
                    }
                }
            }
            OnPropertyChanged(nameof(ListLop));
            CollectionHelper.AddList<object>(PickerCollection, ListLop.Cast<object>().ToList());
            OnPropertyChanged(nameof(PickerCollection));
            #endregion
        }
        private async void FirstLoad(string lopId, int? parentGroupCourseId)
        {
            LoadCommands();

            ListViewFooter = "Đang tải";

            if (parentGroupCourseId != null)
            {
                PickerTitle = "Môn học:";
                var listParentGroupCourse = await GroupCourseUtilities.GetListGroupCourse(lopId, false, null, 1, 1000, (p) => { });
                ListMon = new ObservableCollection<GroupCourseViewModel>();
                CollectionHelper.AddList<GroupCourseViewModel>(ListMon, listParentGroupCourse);
                PickerCollection.Clear();
                CollectionHelper.AddList<object>(PickerCollection, ListMon.Cast<object>().ToList());
                _PickerSelected = ListMon.FirstOrDefault(x => x.Id == parentGroupCourseId); OnPropertyChanged(nameof(PickerSelected));
            }
            else
            {
                #region Load lớp
                ListLop = new ObservableCollection<LopViewModel>();
                var lstLop = await LopUtilities.GetListLop("", (p) => { });
                if (lstLop != null)
                {
                    foreach (var item in lstLop)
                    {
                        if (item != null)
                        {
                            ListLop.Add(item);
                        }
                    }
                }
                OnPropertyChanged(nameof(ListLop));
                #endregion
                CollectionHelper.AddList<object>(PickerCollection, ListLop.Cast<object>().ToList());
                _PickerSelected = ListLop.FirstOrDefault(x => x.LopId == lopId); OnPropertyChanged(nameof(PickerSelected));
            }
            OnPropertyChanged(nameof(PickerCollection));
            LoadGroupCourse(lopId, parentGroupCourseId);
        }

        private async void LoadGroupCourse(string lopId, int? parentId = null)
        {
            if (ListGroupCourse != null)
            {
                ListGroupCourse.Clear();
            }
            ListViewFooter = "Đang tải";
            List<GroupCourseViewModel> lstGroupCourse = await GroupCourseUtilities.GetListGroupCourse(lopId, false, parentId, page, pageSize, (p) => { });
            
            ListGroupCourse = new ObservableCollection<GroupCourseViewModel>();
            CollectionHelper.AddList<GroupCourseViewModel>(ListGroupCourse, lstGroupCourse);
            OnPropertyChanged(nameof(ListGroupCourse));
            if (lstGroupCourse == null || lstGroupCourse.Count == 0)
            {
                ListViewFooter = "Không có khóa học khả dụng";
                return;
            }
            ListViewFooter = "";

            //page = 1; continueLoading = true;
            //ListGroupCourse = new InfiniteScrollCollection<GroupCourseViewModel>
            //{
            //    OnLoadMore = async () =>
            //    {
            //        IsBusy = true;
            //        ListViewFooter = "Đang tải";
            //        page = page + 1;
            //        var listParentGroupCourse = await GroupCourseUtilities.GetListGroupCourse(lopId, false, parentId, page, pageSize, (p) => { });

            //        if (listParentGroupCourse.Count < pageSize)
            //        {
            //            continueLoading = false;
            //            ListViewFooter = "";
            //        }
            //        return listParentGroupCourse;
            //    },

            //    OnCanLoadMore = () =>
            //    {
            //        return continueLoading;
            //    }
            //};

            //var firstListParentGroupCourseLoading = await GroupCourseUtilities.GetListGroupCourse(lopId, false, parentId, page, pageSize, (p) => { });
            //if (firstListParentGroupCourseLoading != null)
            //{
            //    if (firstListParentGroupCourseLoading.Count == 0)
            //    {
            //        ListViewFooter = "Không có khóa học khả dụng";
            //    }
            //    ListGroupCourse.AddRange(firstListParentGroupCourseLoading);
            //    OnPropertyChanged(nameof(ListGroupCourse));
            //}
        }

        private void LoadCommands()
        {
            base.InitializeCommands();
            OnClickSelectLop = new Command<object>((o) =>
            {
                if (o != null)
                {
                    var picker = o as Picker;
                    picker.IsEnabled = true;
                    picker.Focus();
                }
            });
            OnListViewRefreshing = new Command(() =>
            {
                IsRefreshing = true;
                if (PickerSelected.GetType().Equals(typeof(LopViewModel)))
                {
                    LoadGroupCourse(((LopViewModel)PickerSelected)?.LopId);
                }
                else if (PickerSelected.GetType().Equals(typeof(GroupCourseViewModel)))
                {
                    LoadGroupCourse(((GroupCourseViewModel)PickerSelected)?.LopId, ((GroupCourseViewModel)PickerSelected)?.Id);
                }
                
                IsRefreshing = false;
            });
            OnClickGroupCourse = new Command<object>(async (o) =>
            {
                IsBusy = true;
                if (o != null)
                {
                    var model = o as GroupCourseViewModel;

                    if (model?.ParentId != null)
                    {
                        // Có parentId thì đây là nhóm khóa học con
                        // Mở trang danh sách khóa học
                        listCoursePage = new ListCoursePage(model);
                        //if (listCoursePage?.ViewModel == null)
                        //{
                        //    listCoursePage = new ListCoursePage(model);
                        //}
                        LoadPage(listCoursePage);
                    }
                    else
                    {
                        // parentId = null thì có các trường hợp
                        // 1. Đây là nhóm khóa học cha.
                        // 2. Đây là nhóm khóa học bình thường, không cha cũng không con - 28/11/2020 confirm anh Giàu không có
                        // Reload list

                        var newListGroupCoursePage = new ListGroupCoursePage(model.LopId, model.Id);
                        LoadPage(newListGroupCoursePage);
                    }
                }
                else
                {
                    IsBusy = false;
                }
                IsBusy = false;
            });
        }
    }
}
