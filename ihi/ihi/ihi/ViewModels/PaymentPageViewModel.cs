﻿using ihi.Helpers;
using ihi.Interface;
using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace ihi.ViewModels
{
    public class PaymentPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickPayment { get; set; }
        #endregion

        #region Properties
        private PaymentPage _MainWindow;
        public PaymentPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        private ProfileBasesViewModel _Account;
        public ProfileBasesViewModel Account { get => _Account; set { _Account = value; OnPropertyChanged(); } }
        private ObservableCollection<string> _ListAdImage;
        public ObservableCollection<string> ListAdImage { get => _ListAdImage; set { _ListAdImage = value; OnPropertyChanged(); } }
        private decimal _MoneyToBuy;
        public decimal MoneyToBuy { get => _MoneyToBuy; set { _MoneyToBuy = value; OnPropertyChanged(); } }
        #endregion

        public PaymentPageViewModel(PaymentPage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }

        private void FirstLoad()
        {
            LoadCommands();
            Account = constants.Account;
            ListAdImage = new ObservableCollection<string>()
            {
                "banner_150x100.png",
                "banner_150x100.png",
                "banner_150x100.png"
            };
            MoneyToBuy = 20000;
        }


        private void LoadCommands()
        {
            base.InitializeCommands();
            OnClickPayment = new Command(async () =>
            {
                IsBusy = true;
                //string requestId = RandomGenerator.RandomString(15, true);
                //string dataToCreateSignature =
                //                "partnerCode=" + constants.partnerCode +
                //                "&accessKey=" + constants.accessKey +
                //                "&requestId=" + requestId +
                //                "&amount=" + MoneyToBuy.ToString() +
                //                "&orderId=" + requestId +
                //                "&orderInfo=" + $"Payment for {requestId}" +
                //                "&returnUrl=" + constants.MomoReturnUrl +
                //                "&notifyUrl=" + constants.MomoNotifyUrl +
                //                "&extraData=" + "";
                //MomoRequestModel requestMomoModel = new MomoRequestModel()
                //{
                //    accessKey = constants.accessKey,
                //    amount = MoneyToBuy.ToString(),
                //    extraData = "",
                //    notifyUrl = constants.MomoNotifyUrl,
                //    orderId = requestId,
                //    orderInfo = $"Payment for {requestId}",
                //    partnerCode = constants.partnerCode,
                //    requestId = requestId,
                //    requestType = "captureMoMoWallet",
                //    returnUrl = constants.MomoReturnUrl,
                //    signature = CryptographyHelper.signSHA256(dataToCreateSignature, constants.secretKey)
                //};
                //Task tt = new Task(async () =>
                //{
                //    var respone = await PaymentUtilities.RequestMomoFunction(requestMomoModel, (o) => { });
                //    var openMomoDS = DependencyService.Get<IOpenMomo>();
                //    openMomoDS.OpenMomo(respone.deeplink);
                //});
                //tt.Start();

                MomoRequestModel momo = new MomoRequestModel();
                momo.partnerCode = constants.partnerCode;
                momo.accessKey = constants.accessKey;
                momo.orderId = RandomGenerator.RandomString(15, true);
                momo.requestId = momo.orderId;
                momo.amount = MoneyToBuy.ToString();
                momo.orderInfo = $"Thanh toán mua {MoneyToBuy} điểm vào ví IHI.";
                momo.returnUrl = constants.MomoReturnUrl;
                momo.notifyUrl = constants.MomoNotifyUrl;
                momo.extraData = "";

                string rawHash = CryptographyHelper.signSHA256(PaymentUtilities.GetMomoRawHash(momo), constants.secretKey);

                momo.requestType = "captureMoMoWallet";
                momo.signature = rawHash;

                var respone = await PaymentUtilities.RequestMomoFunction(momo, (o) =>
                {
                    MainWindow.DisplayAlert("Cảnh báo", o, "OK");
                    IsBusy = false;
                });
                if (!string.IsNullOrEmpty(respone?.deeplink))
                {
                    var openMomoDS = DependencyService.Get<IOpenMomo>();
                    openMomoDS.OpenMomo(respone.deeplink);
                }
                else
                {
                    await MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau!", "OK");
                    IsBusy = false;
                }

                //MomoRequestModel requestMomoModel = new MomoRequestModel()
                //{
                //    accessKey = constants.accessKey,
                //    amount = MoneyToBuy.ToString(),
                //    extraData = "",
                //    notifyUrl = constants.MomoNotifyUrl,
                //    orderId = Guid.NewGuid().ToString(),
                //    orderInfo = $"Payment for booking id {Guid.NewGuid().ToString()}",
                //    partnerCode = constants.partnerCode,
                //    requestType = "captureMoMoWallet",
                //    returnUrl = constants.MomoReturnUrl,
                //    signature = HMAC_SHA256.ComputeHash(dataToCreateSignature, Cons.secretKey)
                //};
                //requestMomoModel.requestId = requestMomoModel.orderId;
                //Task tt = new Task(async () =>
                //{
                //    var respone = await PaymentUtilities.RequestMomoFunction(momo);
                //    var openMomoDS = DependencyService.Get<IOpenMomo>();
                //    openMomoDS.OpenMomo(respone.deeplink);
                //});
                //tt.Start();
            });
        }
    }
}
