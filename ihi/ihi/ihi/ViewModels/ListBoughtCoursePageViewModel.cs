﻿using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Extended;

namespace ihi.ViewModels
{
    public class ListBoughtCoursePageViewModel : BaseViewModel
    {
        #region Properties
        private CourseDetailPage courseDetailPage;
        private ListBoughtCoursePage _MainWindow;
        public ListBoughtCoursePage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        private InfiniteScrollCollection<CourseBindingModel> _ListCourse;
        public InfiniteScrollCollection<CourseBindingModel> ListCourse { get => _ListCourse; set { _ListCourse = value; OnPropertyChanged(); } }
        private string _ListViewFooter;
        public string ListViewFooter { get => _ListViewFooter; set { _ListViewFooter = value; OnPropertyChanged(); } }
        #endregion
        #region Commands
        public Command<object> OnClickGoToCourseDetail { get; set; }
        #endregion
        public ListBoughtCoursePageViewModel(ListBoughtCoursePage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }

        private async void FirstLoad()
        {
            ListViewFooter = "Đang tải";
            ListCourse = new InfiniteScrollCollection<CourseBindingModel>
            {
                OnLoadMore = async () =>
                {
                    IsBusy = true;
                    var listCourse = await CourseUtilities.GetListBoughtCourse((p) => { });
                    ListViewFooter = "";
                    return listCourse;
                },

                OnCanLoadMore = () =>
                {
                    return false;
                }
            };

            var firstListCourseLoading = await CourseUtilities.GetListBoughtCourse((p) => { });
            if (firstListCourseLoading != null)
            {
                if (firstListCourseLoading.Count == 0)
                {
                    ListViewFooter = "Không có khóa học khả dụng";
                }
                ListCourse.AddRange(firstListCourseLoading);
                OnPropertyChanged(nameof(ListCourse));
            }
            LoadCommand();
        }

        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickGoToCourseDetail = new Command<object>((o) =>
            {
                if (o != null)
                {
                    var course = o as CourseBindingModel;
                    if (course != null)
                    {
                        courseDetailPage = new CourseDetailPage(course, true);
                        LoadPage(courseDetailPage);
                    }
                    else
                    {
                        MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau!", "OK");
                    }
                }
            });
        }
    }
}
