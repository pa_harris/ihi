﻿using ihi.Helpers;
using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Extended;

namespace ihi.ViewModels
{
    public class ListDeThiPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickOpenDeThi { get; set; }
        public ICommand OnClickFocusPicker { get; set; }
        public ICommand OnClickFilter { get; set; }
        #endregion
        #region Properties
        int page = 1; int pageSize = 5; bool continueLoading = true;
        private ListDeThiPage _MainWindow;
        public ListDeThiPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        private EDeThiType _DeThiType;
        public EDeThiType DeThiType { get => _DeThiType; set { _DeThiType = value; OnPropertyChanged(); } }
        private InfiniteScrollCollection<DeThiBindingModel> _ListDeThi;
        public InfiniteScrollCollection<DeThiBindingModel> ListDeThi { get => _ListDeThi; set { _ListDeThi = value; OnPropertyChanged(); } }

        #region Ôn tập
        private ObservableCollection<CourseBindingModel> _ListCourse;
        public ObservableCollection<CourseBindingModel> ListCourse { get => _ListCourse; set { _ListCourse = value; OnPropertyChanged(); } }
        private CourseBindingModel _CourseSelected;
        public CourseBindingModel CourseSelected { get => _CourseSelected; set { _CourseSelected = value; OnSelectNewCourse(); OnPropertyChanged(); } }
        private ObservableCollection<ChuongBindingModel> _ListChuong;
        public ObservableCollection<ChuongBindingModel> ListChuong { get => _ListChuong; set { _ListChuong = value;  OnPropertyChanged(); } }
        private ChuongBindingModel _ChuongSelected;
        public ChuongBindingModel ChuongSelected { get => _ChuongSelected; set { _ChuongSelected = value; OnSelectNewChuong(); OnPropertyChanged(); } }

        private ObservableCollection<BaiGiangBindingModel> _ListBaiGiang;
        public ObservableCollection<BaiGiangBindingModel> ListBaiGiang { get => _ListBaiGiang; set { _ListBaiGiang = value; OnPropertyChanged(); } }
        private BaiGiangBindingModel _BaiGiangSelected;
        public BaiGiangBindingModel BaiGiangSelected { get => _BaiGiangSelected; set { _BaiGiangSelected = value; OnPropertyChanged(); } }
        #endregion

        #region Đề thi thử
        private ObservableCollection<LopViewModel> _ListLop;
        public ObservableCollection<LopViewModel> ListLop { get => _ListLop; set { _ListLop = value; OnPropertyChanged(); } }
        private LopViewModel _LopSelected;
        public LopViewModel LopSelected { get => _LopSelected; set { _LopSelected = value; OnSelectNewLop(); OnPropertyChanged(); } }
        private ObservableCollection<MonViewModel> _ListMon;
        public ObservableCollection<MonViewModel> ListMon { get => _ListMon; set { _ListMon = value; OnPropertyChanged(); } }
        private MonViewModel _MonSelected;
        public MonViewModel MonSelected { get => _MonSelected; set { _MonSelected = value; OnPropertyChanged(); } }
        #endregion


        private async void OnSelectNewLop()
        {
            IsBusy = true;

            // Xóa list đề thi
            ListDeThi?.Clear();

            // Xóa môn
            ListMon?.Clear();
            MonSelected = null;

            // Load môn
            List<MonViewModel> lstMon = await MonUtilities.GetListMon(LopSelected?.LopId, (o) =>
            {
                //MainWindow.DisplayAlert("Cảnh báo", o, "OK");
            });
            ListMon = new ObservableCollection<MonViewModel>();
            CollectionHelper.AddList<MonViewModel>(ListMon, lstMon);
            OnPropertyChanged(nameof(ListMon));

            IsBusy = false;
        }
        private async void OnSelectNewCourse()
        {
            IsBusy = true;

            // Xóa list đề thi
            ListDeThi?.Clear();

            // Xóa chương, bài giảng
            ListChuong?.Clear();
            ChuongSelected = null;
            ListBaiGiang?.Clear();
            BaiGiangSelected = null;

            // Load chương
            List<ChuongBindingModel> lstChuong = await MucLucChuongUtilities.GetMucLucByKhoaHocId(CourseSelected?.KhoahocId, (o) =>
            {
                //MainWindow.DisplayAlert("Cảnh báo", o, "OK");
            });
            ListChuong = new ObservableCollection<ChuongBindingModel>();
            CollectionHelper.AddList<ChuongBindingModel>(ListChuong, lstChuong);
            OnPropertyChanged(nameof(ListChuong));

            IsBusy = false;
        }
        private async void OnSelectNewChuong()
        {
            IsBusy = true;

            // Xóa list đề thi
            ListDeThi?.Clear();

            // Xóa bài giảng
            ListBaiGiang?.Clear();
            BaiGiangSelected = null;

            // Load chương
            List<BaiGiangBindingModel> lstBaiGiang = await BaiGiangUtilities.GetBaiGiangByMucLucId(ChuongSelected?.MuclucId, (o) =>
            {
                //MainWindow.DisplayAlert("Cảnh báo", o, "OK");
            });
            ListBaiGiang = new ObservableCollection<BaiGiangBindingModel>();
            CollectionHelper.AddList<BaiGiangBindingModel>(ListBaiGiang, lstBaiGiang);
            OnPropertyChanged(nameof(ListBaiGiang));

            IsBusy = false;
        }
        #endregion
        public ListDeThiPageViewModel(ListDeThiPage mainWindow, EDeThiType deThiType)
        {
            MainWindow = mainWindow;
            DeThiType = deThiType;
            FirstLoad();
        }

        private async void FirstLoad()
        {
            LoadCommand();
            switch (DeThiType)
            {
                case EDeThiType.OnTap:
                    {
                        List<CourseBindingModel> lstCourse = await CourseUtilities.GetListBoughtCourse((p) => { });
                        ListCourse = new ObservableCollection<CourseBindingModel>();
                        CollectionHelper.AddList<CourseBindingModel>(ListCourse, lstCourse);
                        OnPropertyChanged(nameof(ListCourse));
                    }
                    break;
                case EDeThiType.DeThi:
                    {
                        List<LopViewModel> lstLop = await LopUtilities.GetListLop("", (p) => { });
                        ListLop = new ObservableCollection<LopViewModel>();
                        CollectionHelper.AddList<LopViewModel>(ListLop, lstLop);
                        OnPropertyChanged(nameof(ListLop));
                    }
                    break;
                default:
                    break;
            }
        }

        private async void LoadDeThi()
        {
            page = 1; continueLoading = true;
            GetListDeThiBindingModel model = new GetListDeThiBindingModel();
            model.DeThiType = this.DeThiType;
            model.pageSize = this.pageSize;
            model.BaiGiangId = BaiGiangSelected?.BaigiangId;
            model.LopId = LopSelected?.LopId;
            model.MonId = MonSelected?.MonId;
            model.MucLucId = ChuongSelected?.MuclucId;

            ListDeThi = new InfiniteScrollCollection<DeThiBindingModel>
            {
                OnLoadMore = async () =>
                {
                    IsBusy = true;
                    page = page + 1;
                    model.page = this.page;

                    var lstDeThi = await TracNghiemUtilities.GetListDeThi(model, (p) => { });

                    if (lstDeThi.Count < pageSize)
                    {
                        continueLoading = false;
                    }
                    return lstDeThi;
                },

                OnCanLoadMore = () =>
                {
                    return continueLoading;
                }
            };

            var firstListDeThi = await TracNghiemUtilities.GetListDeThi(model, (p) => { });
            if (firstListDeThi != null)
            {
                ListDeThi.AddRange(firstListDeThi);
                OnPropertyChanged(nameof(ListDeThi));
            }
        }

        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickFocusPicker = new Command<object>((o) =>
            {
                if (o != null)
                {
                    var pck = o as Picker;
                    pck.Focus();
                }
            });
            OnClickFilter = new Command(async () =>
            {
                IsBusy = true;
                if (await ValidateData())
                {
                    LoadDeThi();
                }
                IsBusy = false;
            });
            OnClickOpenDeThi = new Command<object>((o) =>
            {
                IsBusy = true;
                if (o != null)
                {
                    var dethi = o as DeThiBindingModel;
                    if (string.IsNullOrEmpty(dethi?.Id))
                    {
                        MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau!", "OK");
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(dethi.FileDeThi))
                        {
                            LoadPage(new NoFileTracNghiemPage(dethi));
                        }
                        else
                        {
                            LoadPage(new FileTracNghiemPage(dethi));
                        }
                    }
                }
                IsBusy = false;
            });
        }

        private async Task<bool> ValidateData()
        {
            switch (DeThiType)
            {
                case EDeThiType.OnTap:
                    if (string.IsNullOrEmpty(CourseSelected?.KhoahocId))
                    {
                        await MainWindow.DisplayAlert("Cảnh báo", "Vui lòng chọn khóa học!", "OK");
                        return false;
                    }
                    if (string.IsNullOrEmpty(ChuongSelected?.MuclucId))
                    {
                        await MainWindow.DisplayAlert("Cảnh báo", "Vui lòng chọn chương!", "OK");
                        return false;
                    }
                    //if (string.IsNullOrEmpty(BaiGiangSelected?.BaigiangId))
                    //{
                    //    await MainWindow.DisplayAlert("Cảnh báo", "Vui lòng chọn bài giảng!", "OK");
                    //    return false;
                    //}
                    return true;
                case EDeThiType.DeThi:
                    if (string.IsNullOrEmpty(MonSelected?.MonId))
                    {
                        await MainWindow.DisplayAlert("Cảnh báo", "Vui lòng chọn môn!", "OK");
                        return false;
                    }
                    return true;
                default:
                    return false;
            }
        }
    }
}
