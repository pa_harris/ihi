﻿using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace ihi.ViewModels
{
    public class HistoryDetailPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickOpenHistory { get; set; }
        #endregion

        #region Properties
        private HistoryDetailPage _MainWindow;
        public HistoryDetailPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        private DeThiHistoryBindingModel _DeThiHistory;
        public DeThiHistoryBindingModel DeThiHistory { get => _DeThiHistory; set { _DeThiHistory = value; OnPropertyChanged(); } }

        private DeThiHistoryDetailBindingModel _HistoryDetail;
        public DeThiHistoryDetailBindingModel HistoryDetail { get => _HistoryDetail; set { _HistoryDetail = value; OnPropertyChanged(); } }

        public ObservableCollection<FullQuestionWithUserAnswer> ListQuestionWithUserAnswer { get; set; } = new ObservableCollection<FullQuestionWithUserAnswer>();
        #endregion

        public HistoryDetailPageViewModel(HistoryDetailPage mainWindow, DeThiHistoryBindingModel model)
        {
            MainWindow = mainWindow;
            DeThiHistory = model;
            FirstLoad();
        }
        private void FirstLoad()
        {
            LoadCommand();

            // Load đề thi
            LoadHistoryDetail();

        }
        private void LoadCommand()
        {
            base.InitializeCommands();
        }
        private async void LoadHistoryDetail()
        {
            HistoryDetail = await TracNghiemUtilities.GetHistoryDetail(DeThiHistory.HocTapId, (o) =>
            {
                MainWindow.DisplayAlert("Thông báo", o, "OK");
                Application.Current.MainPage.Navigation.PopAsync();
            });
            ListQuestionWithUserAnswer.Clear();
            foreach (var item in HistoryDetail.CauHois)
            {
                FullQuestionWithUserAnswer itemQuestion = new FullQuestionWithUserAnswer();

                itemQuestion.CauHois = item;
                itemQuestion.DapAns = new ObservableCollection<Proc_GetListCauHoiDapAnWithUserDeThi_Result>(HistoryDetail.DapAns.Where((o) => o.CauhoiId == item.CauhoiId).OrderBy((o) => o.Thutu).ToList());
                itemQuestion.DapAns.ForEach((o) => o.Giatri = o.Giatri.Trim());

                ListQuestionWithUserAnswer.Add(itemQuestion);
            }
        }
    }
}
