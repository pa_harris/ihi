﻿using ihi.Helpers;
using ihi.Utilities;
using ihi.Views;
using Plugin.FacebookClient;
using Plugin.GoogleClient;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ihi.ViewModels
{
    public class ChangePasswordPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickChange { get; set; }
        #endregion
        #region Properties
        private ChangePasswordPage _MainWindow;
        public ChangePasswordPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        private string _OldPassword;
        public string OldPassword { get => _OldPassword; set { _OldPassword = value; OnPropertyChanged(); } }
        private string _NewPassword;
        public string NewPassword { get => _NewPassword; set { _NewPassword = value; OnPropertyChanged(); } }
        private string _ConfirmPassword;
        public string ConfirmPassword { get => _ConfirmPassword; set { _ConfirmPassword = value; OnPropertyChanged(); } }
        #endregion
        public ChangePasswordPageViewModel(ChangePasswordPage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }

        private void FirstLoad()
        {
            LoadCommand();
        }
        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickChange = new Command(async () =>
            {
                IsBusy = true;
                if (ValidateData())
                {
                    bool isChangedOK = await AccountUtilities.ChangePassword(new Models.ChangePasswordBindingModel
                    {
                        OldPassword = this.OldPassword,
                        NewPassword = this.NewPassword,
                        ConfirmPassword = this.ConfirmPassword
                    }, (error) =>
                    {
                        MainWindow.DisplayAlert("Cảnh báo", error, "OK");
                    });
                    if (isChangedOK)
                    {
                        await MainWindow.DisplayAlert("Thông báo", "Bạn đã đổi mật khẩu thành công! Vui lòng đăng nhập để sử dụng dịch vụ!", "OK");
                        constants.AccessToken = null;
                        constants.Account = null;
                        CrossFacebookClient.Current.Logout();
                        CrossGoogleClient.Current.Logout();
                        LoadPage(new LoginPage());
                        NavigationHelper.RemoveAllPageBefore();
                    }
                }
                IsBusy = false;
            });
        }
        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(OldPassword))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Mật khẩu cũ không được trống!", "OK");
                return false;
            }
            if (string.IsNullOrEmpty(NewPassword))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Mật khẩu mới không được trống!", "OK");
                return false;
            }
            if (!NewPassword.Equals(ConfirmPassword))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Xác nhận mật khẩu không khớp!", "OK");
                return false;
            }
            return true;
        }
    }
}
