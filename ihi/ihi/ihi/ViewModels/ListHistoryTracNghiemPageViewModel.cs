﻿using ihi.Helpers;
using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ihi.ViewModels
{
    public class ListHistoryTracNghiemPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickOpenHistory { get; set; }
        #endregion

        #region Properties
        private ListHistoryTracNghiemPage _MainWindow;
        public ListHistoryTracNghiemPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }

        private ObservableCollection<DeThiHistoryBindingModel> _ListHistory = new ObservableCollection<DeThiHistoryBindingModel>();
        public ObservableCollection<DeThiHistoryBindingModel> ListHistory { get => _ListHistory; set { _ListHistory = value; OnPropertyChanged(); } }
        #endregion

        public ListHistoryTracNghiemPageViewModel(ListHistoryTracNghiemPage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }
        private async void FirstLoad()
        {
            LoadCommand();

            List<DeThiHistoryBindingModel> lstHistory = await TracNghiemUtilities.GetListHistory((p) => { });
            ListHistory = new ObservableCollection<DeThiHistoryBindingModel>();
            CollectionHelper.AddList<DeThiHistoryBindingModel>(ListHistory, lstHistory);
            OnPropertyChanged(nameof(ListHistory));
        }
        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickOpenHistory = new Command<object>((o) =>
            {
                IsBusy = true;
                if (o != null)
                {
                    DeThiHistoryBindingModel history = o as DeThiHistoryBindingModel;
                    LoadPage(new HistoryDetailPage(history));
                }
            });
        }
    }
}
