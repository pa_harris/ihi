﻿using ihi.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace ihi.ViewModels
{
    public class ExamDetailedPageViewModel : BaseViewModel
    {
        #region Commands

        #endregion
        #region Properties
        private ExamDetailedPage _MainWindow;
        public ExamDetailedPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        #endregion
        public ExamDetailedPageViewModel(ExamDetailedPage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }

        private void FirstLoad()
        {
            LoadCommand();
        }

        private void LoadCommand()
        {
            base.InitializeCommands();
        }
    }
}
