﻿using ihi.Helpers;
using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using ihi.Views.Popups;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Extended;

namespace ihi.ViewModels
{
    public class ListCoursePageViewModel : BaseViewModel
    {
        #region Commands
        public Command<object> OnClickViewInfoCourse { get; set; }
        public Command<object> OnClickPayment { get; set; }
        public ICommand OnListViewRefreshing { get; set; }
        public ICommand OnClickBuy { get; set; }
        #endregion

        #region Properties
        int page = 1; int pageSize = 3; bool continueLoading = true;

        private CourseDetailPage courseDetailPage;
        private ListCoursePage _MainWindow;
        public ListCoursePage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        private string _ListViewFooter;
        public string ListViewFooter { get => _ListViewFooter; set { _ListViewFooter = value; OnPropertyChanged(); } }
        private CourseBindingModel _CourseBuying;
        public CourseBindingModel CourseBuying { get => _CourseBuying; set { _CourseBuying = value; OnPropertyChanged(); } }

        private InfiniteScrollCollection<CourseBindingModel> _ListCourse;
        public InfiniteScrollCollection<CourseBindingModel> ListCourse { get => _ListCourse; set { _ListCourse = value; OnPropertyChanged(); } }
        private GroupCourseViewModel _GroupCourse;
        public GroupCourseViewModel GroupCourse { get => _GroupCourse; set { _GroupCourse = value; OnPropertyChanged(); } }
        private bool _IsRefreshing = false;
        public bool IsRefreshing { get => _IsRefreshing; set { _IsRefreshing = value; OnPropertyChanged(); } }
        #endregion

        public ListCoursePageViewModel(ListCoursePage mainWindow, GroupCourseViewModel groupCourse = null)
        {
            MainWindow = mainWindow;
            GroupCourse = groupCourse;
            FirstLoad();
        }

        private void FirstLoad()
        {
            ListViewFooter = "Đang tải";
            LoadCommands();
            LoadCourse(GroupCourse.Id.ToString());
            CourseBuying = null;
        }

        private async void LoadCourse(string gid)
        {
            page = 1; continueLoading = true;
            ListCourse = new InfiniteScrollCollection<CourseBindingModel>
            {
                OnLoadMore = async () =>
                {
                    IsBusy = true;
                    ListViewFooter = "Đang tải";
                    page = page + 1;
                    var listCourse = await CourseUtilities.GetListCourse(gid, page, pageSize, (p) => { });

                    if (listCourse.Count < pageSize)
                    {
                        continueLoading = false;
                        ListViewFooter = "";
                    }
                    return listCourse;
                },

                OnCanLoadMore = () =>
                {
                    return continueLoading;
                }
            };

            var firstListCourseLoading = await CourseUtilities.GetListCourse(gid, page, pageSize, (p) => { });
            if (firstListCourseLoading != null)
            {
                if (firstListCourseLoading.Count == 0)
                {
                    ListViewFooter = "Không có khóa học khả dụng";
                }
                ListCourse.AddRange(firstListCourseLoading);
                OnPropertyChanged(nameof(ListCourse));
            }
        }
        private void LoadCommands()
        {
            base.InitializeCommands();
            OnClickViewInfoCourse = new Command<object>((o) =>
            {
                IsBusy = true;
                if (o != null)
                {
                    var model = o as CourseBindingModel;

                    if (model != null)
                    {
                        courseDetailPage = new CourseDetailPage(model);
                        LoadPage(courseDetailPage);
                    }
                    else
                    {
                        MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau!", "OK");
                    }
                }
            });
            OnClickPayment = new Command<object>(async (o) =>
            {
                IsBusy = true;
                if (o != null)
                {
                    var model = o as CourseBindingModel;
                    CourseBuying = model;
                    bool isEnough = false;
                    if (model?.Gia == null)
                    {
                        // Giá null = miễn phí
                        isEnough = true;
                    }
                    else
                    {
                        // Giá khác null
                        if (!string.IsNullOrEmpty(constants.AccessToken))
                        {
                            await AccountUtilities.UserInfo((p) => { });
                            if (model?.Chietkhau == null)
                            {
                                // Chiết khấu null kiểm tra theo giá
                                isEnough = constants.Account.Money >= model?.Gia;
                            }
                            else
                            {
                                // Chiết khấu khác null
                                isEnough = constants.Account.Money >= (model?.Gia * (decimal)(model?.Chietkhau / 100));
                            }
                        }
                        else
                        {
                            await MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau.", "OK");
                            IsBusy = false;
                            return;
                        }
                    }

                    if (!isEnough)
                    {
                        await MainWindow.DisplayAlert("Thông báo", "Tiền trong tài khoản của bạn không đủ để thực hiện mua khóa học. Vui lòng nạp thêm tiền để sử dụng dịch vụ.", "OK");
                        IsBusy = false;
                        return;
                    }
                    else
                    {
                        Rg.Plugins.Popup.Services.PopupNavigation.PushAsync(new ConfirmBuyCoursePopup(this));
                    }
                }
                IsBusy = false;
            });
            OnListViewRefreshing = new Command(() =>
            {
                IsRefreshing = true;
                LoadCourse(GroupCourse?.Id.ToString());
                IsRefreshing = false;
            });
            OnClickBuy = new Command(async () =>
            {
                IsBusy = true;
                Rg.Plugins.Popup.Services.PopupNavigation.PopAllAsync();
                if (!string.IsNullOrEmpty(CourseBuying?.KhoahocId))
                {
                    bool isBought = await CourseUtilities.BuyCourse(CourseBuying.KhoahocId, (o) =>
                    {
                        MainWindow.DisplayAlert("Cảnh báo", o, "OK");
                        return;
                    });
                    if (isBought)
                    {
                        MainWindow.DisplayAlert("Thông báo", "Mua khóa học thành công!", "OK");
                        await AccountUtilities.UserInfo((p) => { });
                        LoadPage(new HomePage());
                        NavigationHelper.RemoveAllPageBefore();
                    }
                    //else
                    //{
                    //    MainWindow.DisplayAlert("Cảnh báo", "Mua khóa học không thành công!", "OK");
                    //}
                }
                else
                {
                    MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau!", "OK");
                }
                IsBusy = false;
            });
        }
    }
}
