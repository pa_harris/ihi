﻿using ihi.Utilities;
using ihi.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ihi.ViewModels
{
    public class RegisterPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickRegister { get; set; }
        public ICommand OnClickLogin { get; set; }
        #endregion
        #region Properties
        private LoginPage loginPage;
        private OTPConfirmationPage otpPage;
        private RegisterPage _MainWindow;
        public RegisterPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        private string _ConfirmPassword;
        public string ConfirmPassword { get => _ConfirmPassword; set { _ConfirmPassword = value; OnPropertyChanged(); } }
        private string _Fullname;
        public string Fullname { get => _Fullname; set { _Fullname = value; OnPropertyChanged(); } }
        private string _Username;
        public string Username { get => _Username; set { _Username = value; OnPropertyChanged(); } }
        private string _Password;
        public string Password { get => _Password; set { _Password = value; OnPropertyChanged(); } }
        #endregion
        public RegisterPageViewModel(RegisterPage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }
        private void FirstLoad()
        {
            LoadCommand();
        }
        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickRegister = new Command(async () =>
            {
                IsBusy = true;
                if (ValidateData())
                {
                    bool isDoneRegisterStep1 = await AccountUtilities.Register(new Models.RegisterBindingModel { Email = Username }, (error) =>
                    {
                        MainWindow.DisplayAlert("Cảnh báo", error, "OK");
                        IsBusy = false;
                        return;
                    });
                    if (isDoneRegisterStep1)
                    {
                        await MainWindow.DisplayAlert("Thành công", "Gửi OTP thành công! Vui lòng kiểm tra trong hộp thư!", "OK");
                        if (otpPage == null || otpPage.ViewModel == null)
                        {
                            otpPage = new OTPConfirmationPage(new Models.RegisterModel
                            {
                                Username = this.Username,
                                ConfirmPassword = this.ConfirmPassword,
                                Fullname = this.Fullname,
                                Password = this.Password
                            });
                        }
                        else
                        {
                            if (otpPage.ViewModel.UserName != Username)
                            {
                                otpPage.ViewModel.UserName = Username;
                            }
                        }
                        LoadPage(otpPage);
                    }
                    else
                    {
                        MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng thử lại sau!", "OK");
                        IsBusy = false;
                        return;
                    }
                }
                else
                {
                    IsBusy = false;
                }
            });
            OnClickLogin = new Command(() =>
            {
                IsBusy = true;
                if (loginPage == null)
                {
                    loginPage = new LoginPage(Username);
                }
                LoadPage(loginPage);
            });
        }
        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(Username))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Tên đăng nhập không được trống!", "OK");
                return false;
            }
            if (string.IsNullOrEmpty(Fullname))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Họ tên không được trống!", "OK");
                return false;
            }
            if (string.IsNullOrEmpty(Password))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Mật khẩu không được trống!", "OK");
                return false;
            }
            if (string.IsNullOrEmpty(ConfirmPassword))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Xác nhận mật khẩu không được trống!", "OK");
                return false;
            }
            if (!Password.Equals(ConfirmPassword))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Mật khẩu không trùng khớp!", "OK");
                return false;
            }
            return true;
        }
    }
}
