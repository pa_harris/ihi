﻿using ihi.Helpers;
using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using Plugin.FacebookClient;
using Plugin.GoogleClient;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace ihi.ViewModels
{
    public class UserSettingPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickBack { get; set; }
        public ICommand OnClickChangeAvatar { get; set; }
        public ICommand OnClickTakePhoto { get; set; }
        public ICommand OnClickChoosePicture { get; set; }
        public ICommand OnClickLogout { get; set; }
        public ICommand OnClickUpdateUserInfo { get; set; }
        public ICommand OnClickChangePassword { get; set; }
        #endregion

        #region Properties
        private ChangePasswordPage changePasswordPage;
        private UpdateUserInfoPage updateUserInfoPage;

        private UserSettingPage _MainWindow;
        public UserSettingPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        private ProfileBasesViewModel _Account;
        public ProfileBasesViewModel Account { get => _Account; set { _Account = value; OnPropertyChanged(); } }
        #endregion

        public UserSettingPageViewModel(UserSettingPage mainWindow)
        {
            MainWindow = mainWindow;
            FirstLoad();
        }

        private void FirstLoad()
        {
            LoadCommands();
            Account = constants.Account;
        }

        private void ChangeAvatar(MediaFile picture)
        {
            IsBusy = true;
            SKBitmap bitmap = SKBitmap.Decode(picture.GetStream());

            #region Crop
            PhotoCroppingPage croppingPage = new PhotoCroppingPage(picture, bitmap);
            croppingPage.ReturnCropped += CroppingPage_ReturnCropped;
            LoadPage(croppingPage);
            #endregion           
        }

        private void CroppingPage_ReturnCropped(MediaFile picture, SKBitmap obj)
        {
            RequestChangeAvatar(picture, obj);
        }

        private async void RequestChangeAvatar(MediaFile picture, SKBitmap bitmap)
        {
            SKImage image = SKImage.FromBitmap(bitmap);
            SKData sKData = image.Encode();
            string newAvatar = await AccountUtilities.ChangeAvatar(sKData, picture, (o) =>
            {
                MainWindow.DisplayAlert("Cảnh báo", o, "OK");
            });
            if (!string.IsNullOrEmpty(newAvatar))
            {
                await MainWindow.DisplayAlert("Thông báo", "Đổi ảnh đại diện thành công!", "OK");
                string newAva = constants.DomainImageUrl + newAvatar;
                this.Account.Avatar = newAva; OnPropertyChanged(nameof(this.Account));
                constants.Account.Avatar = newAva;
            }
            else
            {
                await MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau!", "OK");
            }
        }

        private void LoadCommands()
        {
            base.InitializeCommands();
            OnClickBack = new Command(async () =>
            {
                await Application.Current.MainPage.Navigation.PopAsync();
            });
            OnClickChangeAvatar = new Command(async () =>
            {
                IsBusy = true;
                string action = await MainWindow.DisplayActionSheet("Đổi ảnh đại diện", "Thoát", null,
                    "Chụp ảnh mới",
                    "Chọn từ bộ sưu tập");
                if (action == "Chụp ảnh mới")
                {
                    OnClickTakePhoto.Execute(new object());
                }
                else if (action == "Chọn từ bộ sưu tập")
                {
                    OnClickChoosePicture.Execute(new object());
                }
                IsBusy = false;
            });
            OnClickTakePhoto = new Command<object>(async (p) =>
            {
                await CrossMedia.Current.Initialize();

                var status = await CrossPermissions.Current.CheckPermissionStatusAsync<CameraPermission>();
                if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
                    {
                        await MainWindow.DisplayAlert("Cảnh báo", "Chấp nhận quyền sử dụng camera để sử dụng tính năng này!", "OK");
                    }

                    status = await CrossPermissions.Current.RequestPermissionAsync<CameraPermission>();
                }

                if (status == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    //Query permission
                    status = await CrossPermissions.Current.RequestPermissionAsync<StoragePermission>();
                    if (status == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                    {
                        if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                        {
                            await MainWindow.DisplayAlert("No Camera", ":( No camera available.", "OK");
                            return;
                        }
                        var picture = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
                        {
                            DefaultCamera = Plugin.Media.Abstractions.CameraDevice.Front,
                            PhotoSize = PhotoSize.MaxWidthHeight,
                            CustomPhotoSize = 40,
                            AllowCropping = true,
                            CompressionQuality = 85,
                            
                        });
                        IsBusy = true;
                        if (picture == null)
                        {
                            await MainWindow.DisplayAlert("Thông báo", "Đã có lỗi xảy ra! Vui lòng thử lại sau!", "OK");
                            IsBusy = false;
                            return;
                        }

                        ChangeAvatar(picture);
                    }
                }
            }, p => true);
            OnClickChoosePicture = new Command<object>(async (p) =>
            {
                IsBusy = true;
                await CrossMedia.Current.Initialize();

                var status = await CrossPermissions.Current.CheckPermissionStatusAsync<StoragePermission>();
                if (status != Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Location))
                    {
                        await MainWindow.DisplayAlert("Cảnh báo", "Chấp nhận quyền sử dụng bộ nhớ để sử dụng tính năng này!", "OK");
                    }

                    status = await CrossPermissions.Current.RequestPermissionAsync<StoragePermission>();
                }

                if (status == Plugin.Permissions.Abstractions.PermissionStatus.Granted)
                {
                    //Query permission
                    if (!CrossMedia.Current.IsPickPhotoSupported)
                    {
                        await MainWindow.DisplayAlert("No PickPhoto", ":( No PickPhoto available.", "OK");
                        IsBusy = false;
                        return;
                    }
                    var picture = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions()
                    {
                        PhotoSize = PhotoSize.Custom,
                        CustomPhotoSize = 50,
                        CompressionQuality = 85
                    });

                    IsBusy = true;

                    if (picture == null)
                    {
                        await MainWindow.DisplayAlert("Thông báo", "Đã có lỗi xảy ra! Vui lòng thử lại sau!", "OK");
                        IsBusy = false;
                        return;
                    }

                    ChangeAvatar(picture);
                }
            }, p => true);
            OnClickLogout = new Command(() =>
            {
                IsBusy = true;
                constants.AccessToken = null;
                constants.Account = null;
                CrossFacebookClient.Current.Logout();
                CrossGoogleClient.Current.Logout();
                LoadPage(new LoginPage());
                NavigationHelper.RemoveAllPageBefore();
            });
            OnClickUpdateUserInfo = new Command(() =>
            {
                IsBusy = true;
                if (updateUserInfoPage?.ViewModel == null)
                {
                    updateUserInfoPage = new UpdateUserInfoPage();
                }
                LoadPage(updateUserInfoPage);
            });
            OnClickChangePassword = new Command(() =>
            {
                IsBusy = true;
                if (changePasswordPage?.ViewModel == null)
                {
                    changePasswordPage = new ChangePasswordPage();
                }
                LoadPage(changePasswordPage);
            });
        }
    }
}
