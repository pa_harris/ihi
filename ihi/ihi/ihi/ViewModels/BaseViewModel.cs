﻿using ihi.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using Xamarin.Forms;

namespace ihi.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        #region Properties
        private bool _IsBusy;
        public bool IsBusy { get => _IsBusy; set { _IsBusy = value; OnPropertyChanged(); } }
        #endregion
        #region Commands
        public Command<object> PageAppearing { get; set; }
        #endregion

        #region Function
        public void InitializeCommands()
        {
            PageAppearing = new Command<object>((o) =>
            {
                var page = o as Page;
                IsBusy = false;
                if (page != null)
                {

                }
            });
        }
        public void LoadPage(Page page)
        {
            try
            {
                Application.Current.MainPage.Navigation.PushAsync(page);
            }
            catch (Exception ee)
            {

            }
        }
        #endregion





        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
