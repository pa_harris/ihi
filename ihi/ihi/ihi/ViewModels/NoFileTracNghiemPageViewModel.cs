﻿using ihi.Models;
using ihi.Utilities;
using ihi.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Internals;

namespace ihi.ViewModels
{
    public class NoFileTracNghiemPageViewModel : BaseViewModel
    {
        #region Commands
        public ICommand OnClickNopBai { get; set; }
        public ICommand OnSelectDapAns { get; set; }
        #endregion
        #region Properties
        private NoFileTracNghiemPage _MainWindow;
        public NoFileTracNghiemPage MainWindow { get => _MainWindow; set { _MainWindow = value; OnPropertyChanged(); } }
        private DeThiBindingModel _DeThi;
        public DeThiBindingModel DeThi { get => _DeThi; set { _DeThi = value; OnPropertyChanged(); } }
        private DeThiDetailBindingModel _DeThiDetail;
        public DeThiDetailBindingModel DeThiDetail { get => _DeThiDetail; set { _DeThiDetail = value; OnPropertyChanged(); } }
        public ObservableCollection<FullQuestion> ListQuestion { get; set; } = new ObservableCollection<FullQuestion>();
        private List<DapAnSubmitModel> listDapAn = new List<DapAnSubmitModel>();

        public int TimerMinute { get; set; } = 0;
        public int TimerSecond { get; set; } = 0;
        #endregion
        public NoFileTracNghiemPageViewModel(NoFileTracNghiemPage mainWindow, DeThiBindingModel dethi)
        {
            MainWindow = mainWindow;
            DeThi = dethi;
            FirstLoad();
            if (string.IsNullOrEmpty(dethi?.Id))
            {
                MainWindow.DisplayAlert("Cảnh báo", "Đã có lỗi xảy ra! Vui lòng quay lại sau!", "OK");
                Application.Current.MainPage.Navigation.PopAsync();
            }
        }

        private void FirstLoad()
        {
            ListQuestion.Clear();
            TimerMinute = DeThi.Thoigianthi;
            LoadCommand();
            Device.StartTimer(new TimeSpan(0, 0, 1), () =>
            {
                if (TimerSecond > 0)
                {
                    TimerSecond--;
                    OnPropertyChanged(nameof(TimerSecond));
                }
                else
                {
                    TimerSecond = 59;
                    OnPropertyChanged(nameof(TimerSecond));
                    TimerMinute--;
                    OnPropertyChanged(nameof(TimerMinute));
                }
                if (TimerMinute == 0 && TimerSecond == 0)
                {
                    // hết giờ thi
                    MainWindow.DisplayAlert("Cảnh báo", "Đã hết giờ thi!", "OK");
                    return false;
                }
                else
                {
                    return true;
                }
            });
            LoadDeThiDetail();
        }

        private async void LoadDeThiDetail()
        {
            DeThiDetail = await TracNghiemUtilities.GetDeThiDetail((DeThi.DeThiType == EDeThiType.OnTap) ? DeThi.Id : null, (DeThi.DeThiType == EDeThiType.OnTap) ? null : DeThi.Id, (o) =>
            {
                MainWindow.DisplayAlert("Thông báo", o, "OK");
                Application.Current.MainPage.Navigation.PopAsync();
            });
            ListQuestion.Clear();
            foreach (var item in DeThiDetail.CauHois)
            {
                FullQuestion itemQuestion = new FullQuestion();

                itemQuestion.CauHois = item;
                itemQuestion.DapAns = new ObservableCollection<Proc_GetListCauHoiDapAnDeThi_Result>(DeThiDetail.DapAns.Where((o) => o.CauhoiId == item.CauhoiId).OrderBy((o) => o.Thutu).ToList());
                itemQuestion.DapAns.ForEach((o) => o.Giatri = o.Giatri.Trim());

                ListQuestion.Add(itemQuestion);
            }
            //listDapAn = new List<DapAnSubmitModel>(ListQuestion.Select(x => new DapAnSubmitModel
            //{
            //    CauHoiId = x.CauHois.CauhoiId,
            //    DapAn = x.DapAns.Where(y => y.UserDapAn == true).FirstOrDefault().Thutu
            //}));
        }

        private void LoadCommand()
        {
            base.InitializeCommands();
            OnClickNopBai = new Command(async () =>
            {
                IsBusy = true;
                listDapAn = new List<DapAnSubmitModel>(ListQuestion.Select(x => new DapAnSubmitModel
                {
                    CauHoiId = x.CauHois.CauhoiId,
                    DapAn = x.DapAns.Where(y => y.UserDapAn == true).FirstOrDefault()?.Thutu
                }));

                // nộp bài
                var point = await TracNghiemUtilities.SubmitAnswer(DeThi.DeThiType == EDeThiType.OnTap ? DeThi.Id : null, DeThi.DeThiType == EDeThiType.OnTap ? null : DeThi.Id, listDapAn, (o) => 
                {
                    MainWindow.DisplayAlert("Thông báo", o, "OK");
                });
                if (point == -1)
                {
                    MainWindow.DisplayAlert("Thông báo", $"Nộp bài không thành công!", "OK");
                }
                else
                {
                    MainWindow.DisplayAlert("Thông báo", $"Nộp bài thành công! Điểm của bạn là: {point}.", "OK");
                }

                IsBusy = false;
            });
            OnSelectDapAns = new Command<object>((o) =>
            {
                if (o != null)
                {
                    var stkDapAn = o as StackLayout;
                    var flxListDapAn = stkDapAn.Parent as FlexLayout;
                    foreach (var item in flxListDapAn.Children)
                    {
                        CheckBox checkBox = (item as StackLayout).Children.FirstOrDefault() as CheckBox;
                        checkBox.IsChecked = false;
                    }

                    (stkDapAn.Children.FirstOrDefault() as CheckBox).IsChecked = true;
                }
            });
        }
    }
}
