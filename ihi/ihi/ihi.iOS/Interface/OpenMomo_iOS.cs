﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

using Foundation;
using ihi.Interface;
using ihi.iOS.Interface;
using UIKit;
using Xamarin.Forms;
[assembly: Xamarin.Forms.Dependency(typeof(OpenMomo_iOS))]
namespace ihi.iOS.Interface
{
    public class OpenMomo_iOS : IOpenMomo
    {
        public OpenMomo_iOS() { }

        public void OpenMomo(string deeplink)
        {
            NSUrl request = new NSUrl(new System.Uri(deeplink).AbsoluteUri);
            try
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    bool isOpened = UIApplication.SharedApplication.OpenUrl(request);

                    if (isOpened == false)
                        UIApplication.SharedApplication.OpenUrl(new NSUrl("yourappurl"));
                });
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Cannot open url: {0}, Error: {1}", request.AbsoluteString, ex.Message);
                var alertView = new UIAlertView("Error", ex.Message, null, "OK", null);

                alertView.Show();
            }
        }
    }    
}