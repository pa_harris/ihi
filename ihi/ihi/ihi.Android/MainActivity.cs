﻿
using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using FFImageLoading.Forms.Platform;
using Plugin.FacebookClient;
using Plugin.GoogleClient;
using Plugin.Permissions;
using MediaManager;
using MediaManager.Forms.Platforms.Android;
using Plugin.FirebasePushNotification;
using Android.Content;

namespace ihi.Droid
{
    [Activity(Label = "ihi", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize )]
    [IntentFilter(new[] { Android.Content.Intent.ActionView }, DataScheme = "ihi", DataHost = "ihi.vn", AutoVerify = true, DataPathPrefix = "/", Categories = new[] { Android.Content.Intent.CategoryDefault, Android.Content.Intent.CategoryBrowsable })]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            Rg.Plugins.Popup.Popup.Init(this, savedInstanceState);

            base.OnCreate(savedInstanceState);

            #region Facebook key hashes
            //try
            //{
            //    PackageInfo info = Android.App.Application.Context.PackageManager.GetPackageInfo(Android.App.Application.Context.PackageName, PackageInfoFlags.Signatures);
            //    foreach (var signature in info.Signatures)
            //    {
            //        MessageDigest md = MessageDigest.GetInstance("SHA");
            //        md.Update(signature.ToByteArray());

            //        System.Diagnostics.Debug.WriteLine(Convert.ToBase64String(md.Digest()));
            //    }
            //}
            //catch (NoSuchAlgorithmException e)
            //{
            //    System.Diagnostics.Debug.WriteLine(e);
            //}
            //catch (Exception e)
            //{
            //    System.Diagnostics.Debug.WriteLine(e);
            //}
            #endregion
            #region Google key hashes
            //try
            //{
            //    PackageInfo info = Android.App.Application.Context.PackageManager.GetPackageInfo(Android.App.Application.Context.PackageName, PackageInfoFlags.Signatures);
            //    foreach (var signature in info.Signatures)
            //    {
            //        MessageDigest md = MessageDigest.GetInstance("SHA");
            //        md.Update(signature.ToByteArray());

            //        System.Diagnostics.Debug.WriteLine(BitConverter.ToString(md.Digest()).Replace("-", ":"));
            //    }
            //}
            //catch (NoSuchAlgorithmException e)
            //{
            //    System.Diagnostics.Debug.WriteLine(e);
            //}
            //catch (Exception e)
            //{
            //    System.Diagnostics.Debug.WriteLine(e);
            //}
            #endregion

            #region Init library
            FacebookClientManager.Initialize(this);
            GoogleClientManager.Initialize(this);
            CachedImageRenderer.Init(enableFastRenderer: true);
            CrossMediaManager.Current.Init(this);
            CrossMediaManager.Current.Reactive();
            FirebasePushNotificationManager.ProcessIntent(this, Intent);
            #endregion

            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            LoadApplication(new App());
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Android.Content.Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            FacebookClientManager.OnActivityResult(requestCode, resultCode, data);
            GoogleClientManager.OnAuthCompleted(requestCode, resultCode, data);
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        protected override void OnNewIntent(Intent intent)
        {
            base.OnNewIntent(intent);
            FirebasePushNotificationManager.ProcessIntent(this, intent);
        }

        public override void OnBackPressed()
        {
            if (Rg.Plugins.Popup.Popup.SendBackPressed(base.OnBackPressed))
            {
                // Do something if there are some pages in the `PopupStack`
            }
            else
            {
                // Do something if there are not any pages in the `PopupStack`
            }
        }
    }
}