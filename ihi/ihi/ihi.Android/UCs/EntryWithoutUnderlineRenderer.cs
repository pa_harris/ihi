﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using static Java.Util.ResourceBundle;

using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using ihi.UCs;
using ihi.Droid.UCs;
using Android.Text;

[assembly: ExportRenderer(typeof(EntryWithoutUnderline), typeof(EntryWithoutUnderlineRenderer))]
namespace ihi.Droid.UCs
{
    public class EntryWithoutUnderlineRenderer : EntryRenderer
    {
        public EntryWithoutUnderlineRenderer(Context context) : base(context)
        {
        }

        [Obsolete]
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                GradientDrawable gd = new GradientDrawable();
                gd.SetColor(global::Android.Graphics.Color.Transparent);
                this.Control.SetBackgroundDrawable(gd);
                Control.SetHintTextColor(ColorStateList.ValueOf(global::Android.Graphics.Color.Gray));
            }
        }
    }
}