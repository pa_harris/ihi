﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using ihi.Droid.UCs;
using ihi.UCs;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using WebView = Android.Webkit.WebView;
[assembly: ExportRenderer(typeof(AutoHeightWebview), typeof(AutoHeightWebviewRenderer))]
namespace ihi.Droid.UCs
{
    public class AutoHeightWebviewRenderer : WebViewRenderer
    {
        WebView _webView;

        public AutoHeightWebviewRenderer(Context context) : base(context)
        {

        }

        class AutoHeightWebViewClient : Android.Webkit.WebViewClient
        {
            AutoHeightWebviewRenderer _renderer;
            private readonly AutoHeightWebview _xWebView;
            string _lastUrlNotOverridden;

            public AutoHeightWebViewClient(AutoHeightWebviewRenderer renderer, AutoHeightWebview xWebView)
            {
                try
                {
                    //_renderer = renderer ?? throw new ArgumentNullException("renderer");
                    if (renderer == null || renderer.ElementController == null)
                    {
                        renderer = new AutoHeightWebviewRenderer(Android.App.Application.Context);
                        Console.WriteLine(Android.App.Application.Context.GetActivity().ToString());
                    }
                    _renderer = renderer;
                    _xWebView = xWebView;
                }
                catch (Exception ee)
                {

                }
            }

            public override async void OnPageFinished(WebView view, string url)
            {
                try
                {
                    if (_xWebView != null)
                    {
                        int i = 10;
                        while (view.ContentHeight == 0 && i-- > 0) // wait here till content is rendered
                            await System.Threading.Tasks.Task.Delay(1);

                        if (view != null)
                        {
                            _xWebView.HeightRequest = view.ContentHeight;
                            _xWebView.VerticalOptions = LayoutOptions.Start;
                            _xWebView.BackgroundColor = System.Drawing.Color.Transparent;
                            view.Settings.TextZoom = 100;
                            view.SetBackgroundColor(Android.Graphics.Color.Transparent);
                        }

                        //Console.WriteLine(view.ContentDescription);
                    }
                    base.OnPageFinished(view, url);

                    var source = new UrlWebViewSource { Url = url };
                    var args = new WebNavigatedEventArgs(WebNavigationEvent.NewPage, source, url, WebNavigationResult.Success);
                    _renderer.ElementController.SendNavigated(args);
                }
                catch (Exception ee)
                {

                }
            }

            public override void OnPageStarted(WebView view, string url, Bitmap favicon)
            {
                try
                {
                    base.OnPageStarted(view, url, favicon);

                    var args = new WebNavigatingEventArgs(WebNavigationEvent.NewPage, new UrlWebViewSource { Url = url }, url);
                    if (_renderer != null)
                    {
                        _renderer.ElementController.SendNavigating(args);
                    }
                }
                catch (Exception ee)
                {
                }
            }

            public override bool ShouldOverrideUrlLoading(WebView view, IWebResourceRequest request)
            {
                _lastUrlNotOverridden = null;
                if (_renderer?.Element == null || request?.Url == null)
                    return true;
                var url = request.Url.ToString();
                if (url == WebViewRenderer.AssetBaseUrl)
                    return false;

                var args = new WebNavigatingEventArgs(WebNavigationEvent.NewPage, new UrlWebViewSource { Url = url }, url);
                _renderer.ElementController.SendNavigating(args);
                _lastUrlNotOverridden = args.Cancel ? null : url;

                return args.Cancel;
            }

        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.WebView> e)
        {
            try
            {
                base.OnElementChanged(e);
                _webView = Control;

                if (e.OldElement == null)
                {
                    Control.SetWebViewClient(new AutoHeightWebViewClient(this, e.NewElement as AutoHeightWebview));
                    var nativeWebView = Control;
                    nativeWebView.Settings.JavaScriptEnabled = true;
                }
            }
            catch (System.ObjectDisposedException)
            {
            }
        }
    }
}