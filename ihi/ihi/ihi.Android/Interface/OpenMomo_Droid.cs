﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ihi.Droid.Interface;
using ihi.Interface;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(OpenMomo_Droid))]
namespace ihi.Droid.Interface
{
    public class OpenMomo_Droid : IOpenMomo
    {
        public OpenMomo_Droid() { }

        public void OpenMomo(string deeplink)
        {
            Intent intentToMomoApp = new Intent(Intent.ActionView, Android.Net.Uri.Parse(deeplink));
            Forms.Context.StartActivity(Intent.CreateChooser(intentToMomoApp, "Open Momo App"));
        }
    }
}