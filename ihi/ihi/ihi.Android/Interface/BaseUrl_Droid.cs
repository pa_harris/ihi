﻿using ihi.Droid.Interface;
using ihi.Interface;
using Xamarin.Forms;

[assembly: Dependency(typeof(BaseUrl_Droid))]
namespace ihi.Droid.Interface
{
    public class BaseUrl_Droid : IBaseUrl
    {
        public string Get()
        {
            return "file:///android_asset/";
        }
    }
}